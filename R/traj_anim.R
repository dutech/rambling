## *****************************************************************************
# Build a 'fig' that plot robot 2D trajectory (x,y) with animation
#
# usage (in R):
# > source("traj_anim.R")
# > fig

## need plot_ly
require(plotly)
## utility to read "trace" data files
source( file = "plot_cxsom.R")

## data to plot
## For every row: t pos0(=x) pos1(=y) orient ...

dd <- load_headerfile( "trace_simu.csv" )
nb_row <- nrow(dd)
print( paste0("nb_row=", nb_row))

## plotly figure for Robot Trajectory
fig_traj <- plot_ly()

# creates frames ***************************************************************
# each frame plot (x,y)

fig_traj <- add_paths(fig_traj, x=as.numeric(dd$pos0), y=as.numeric(dd$pos1),
                 color="Traj2D")
fig_traj <- add_markers( fig_traj,x=as.numeric(dd$pos0), y=as.numeric(dd$pos1),
                    frame=as.numeric(dd$t), color="PosXY" )
# orientation, sensors (need to de-normalize)
sensor_max_detect <- 4.0
robot_radius <- 0.25
posX <- as.numeric(dd$pos0)
posY <- as.numeric(dd$pos1)
orient <- as.numeric(dd$orient)
obsL <- robot_radius + (as.numeric(dd$obs_l) + 1.0 ) / 2.0 * sensor_max_detect
obsC <- robot_radius + (as.numeric(dd$obs_c) + 1.0 ) / 2.0 * sensor_max_detect
obsR <- robot_radius + (as.numeric(dd$obs_r) + 1.0 ) / 2.0 * sensor_max_detect
## fig_traj <- add_segments( fig_traj,
##                     x=as.numeric(dd$pos0),
##                     xend=as.numeric(dd$pos0)+cos(as.numeric(dd$orient)),
##                     y=as.numeric(dd$pos1),
##                     yend=as.numeric(dd$pos1)+sin(as.numeric(dd$orient)),
##                     color="orient", name="orient",
##                     frame=as.numeric(dd$t) )
fig_traj <- add_segments( fig_traj,
                    x=posX+robot_radius*cos(pi/4+orient),
                    xend=posX+obsL*cos(pi/4+orient),
                    y=posY+robot_radius*sin(pi/4+orient),
                    yend=posY+obsL*sin(pi/4+orient),
                    color="obs", name="obsL",
                    frame=as.numeric(dd$t) )
fig_traj <- add_segments( fig_traj,
                    x=posX+robot_radius*cos(orient),
                    xend=posX+obsC*cos(orient),
                    y=posY+robot_radius*sin(orient),
                    yend=posY+obsC*sin(orient),
                    color="obs", name="obsC",
                    frame=as.numeric(dd$t) )
fig_traj <- add_segments( fig_traj,
                    x=posX+robot_radius*cos(-pi/4+orient),
                    xend=posX+obsR*cos(-pi/4+orient),
                    y=posY+robot_radius*sin(-pi/4+orient),
                    yend=posY+obsR*sin(-pi/4+orient),
                    color="obs", name="obsR",
                    frame=as.numeric(dd$t) )
# fig_traj <- animation_opts(fig_traj, frame=100, transition=0)

## plotly figure for Robot speed
fig_spd <- plot_ly()

# creates frames ***************************************************************
# each frame plot (x,y)

fig_spd <- add_paths(fig_spd, x=as.numeric(dd$t), y=as.numeric(dd$lin_spd),
                     color="LinSpd")
min_lin_spd <- min(dd$lin_spd)
max_lin_spd <- max(dd$lin_spd)
lin_spd_len <- 0.05 * (max_lin_spd - min_lin_spd)
fig_spd <- add_segments( fig_spd,
                        x=as.numeric(dd$t),
                        xend=as.numeric(dd$t),
                        y=as.numeric(dd$lin_spd) - lin_spd_len,
                        yend=as.numeric(dd$lin_spd) + lin_spd_len,
                        color="PosXY", name="t"    ,
                        frame=as.numeric(dd$t) )

## fig_spd <- add_markers(fig_spd, x=as.numeric(dd$t), y=as.numeric(dd$lin_spd),
##                     frame=as.numeric(dd$t), color="PosXY" )

## fig_spd <- animation_opts(fig_spd, frame=100, transition=0)

# subplots side to side
fig <- subplot( fig_spd, fig_traj, shareX=FALSE, shareY=FALSE )
fig <- animation_opts(fig, frame=100, transition=0)
