List of 8
 $ x            :List of 6
  ..$ visdat  :List of 1
  .. ..$ 920a22cc32e1:function ()  
  ..$ cur_data: chr "920a22cc32e1"
  ..$ attrs   :List of 3
  .. ..$ 920a22cc32e1:List of 3
  .. .. ..$ alpha_stroke: num 1
  .. .. ..$ sizes       : num [1:2] 10 100
  .. .. ..$ spans       : num [1:2] 1 20
  .. ..$ 920a22cc32e1:List of 9
  .. .. ..$ alpha_stroke: num 1
  .. .. ..$ sizes       : num [1:2] 10 100
  .. .. ..$ spans       : num [1:2] 1 20
  .. .. ..$ x           :Class 'formula'  language ~x
  .. .. .. .. ..- attr(*, ".Environment")=<environment: 0x558d591f8968> 
  .. .. ..$ y           :Class 'formula'  language ~y
  .. .. .. .. ..- attr(*, ".Environment")=<environment: 0x558d591f8968> 
  .. .. ..$ type        : chr "scatter"
  .. .. ..$ mode        : chr "lines"
  .. .. ..$ name        : chr "hypo"
  .. .. ..$ inherit     : logi TRUE
  .. .. ..- attr(*, "class")= chr [1:2] "plotly_path" "list"
  .. ..$ 920a22cc32e1:List of 10
  .. .. ..$ alpha_stroke: num 1
  .. .. ..$ sizes       : num [1:2] 10 100
  .. .. ..$ spans       : num [1:2] 1 20
  .. .. ..$ x           :Class 'formula'  language ~x
  .. .. .. .. ..- attr(*, ".Environment")=<environment: R_GlobalEnv> 
  .. .. ..$ y           :Class 'formula'  language ~y
  .. .. .. .. ..- attr(*, ".Environment")=<environment: R_GlobalEnv> 
  .. .. ..$ type        : chr "scatter"
  .. .. ..$ mode        : chr "markers"
  .. .. ..$ frame       : num [1:50] 0 0.128 0.256 0.385 0.513 ...
  .. .. ..$ color       : chr "PosXY"
  .. .. ..$ inherit     : logi TRUE
  ..$ layout  :List of 3
  .. ..$ width : NULL
  .. ..$ height: NULL
  .. ..$ margin:List of 4
  .. .. ..$ b: num 40
  .. .. ..$ l: num 60
  .. .. ..$ t: num 25
  .. .. ..$ r: num 10
  ..$ source  : chr "A"
  ..$ config  :List of 2
  .. ..$ modeBarButtonsToAdd: chr [1:2] "hoverclosest" "hovercompare"
  .. ..$ showSendToCloud    : logi FALSE
  ..- attr(*, "TOJSON_FUNC")=function (x, ...)  
 $ width        : NULL
 $ height       : NULL
 $ sizingPolicy :List of 7
  ..$ defaultWidth : chr "100%"
  ..$ defaultHeight: num 400
  ..$ padding      : num 0
  ..$ fill         : NULL
  ..$ viewer       :List of 6
  .. ..$ defaultWidth : NULL
  .. ..$ defaultHeight: NULL
  .. ..$ padding      : NULL
  .. ..$ fill         : logi TRUE
  .. ..$ suppress     : logi FALSE
  .. ..$ paneHeight   : NULL
  ..$ browser      :List of 5
  .. ..$ defaultWidth : NULL
  .. ..$ defaultHeight: NULL
  .. ..$ padding      : NULL
  .. ..$ fill         : logi TRUE
  .. ..$ external     : logi FALSE
  ..$ knitr        :List of 3
  .. ..$ defaultWidth : NULL
  .. ..$ defaultHeight: NULL
  .. ..$ figure       : logi TRUE
 $ dependencies :List of 5
  ..$ :List of 10
  .. ..$ name      : chr "typedarray"
  .. ..$ version   : chr "0.1"
  .. ..$ src       :List of 1
  .. .. ..$ file: chr "htmlwidgets/lib/typedarray"
  .. ..$ meta      : NULL
  .. ..$ script    : chr "typedarray.min.js"
  .. ..$ stylesheet: NULL
  .. ..$ head      : NULL
  .. ..$ attachment: NULL
  .. ..$ package   : chr "plotly"
  .. ..$ all_files : logi FALSE
  .. ..- attr(*, "class")= chr "html_dependency"
  ..$ :List of 10
  .. ..$ name      : chr "jquery"
  .. ..$ version   : chr "3.5.1"
  .. ..$ src       :List of 1
  .. .. ..$ file: chr "lib/jquery"
  .. ..$ meta      : NULL
  .. ..$ script    : chr "jquery.min.js"
  .. ..$ stylesheet: NULL
  .. ..$ head      : NULL
  .. ..$ attachment: NULL
  .. ..$ package   : chr "crosstalk"
  .. ..$ all_files : logi TRUE
  .. ..- attr(*, "class")= chr "html_dependency"
  ..$ :List of 10
  .. ..$ name      : chr "crosstalk"
  .. ..$ version   : chr "1.2.1"
  .. ..$ src       :List of 1
  .. .. ..$ file: chr "www"
  .. ..$ meta      : NULL
  .. ..$ script    : chr "js/crosstalk.min.js"
  .. ..$ stylesheet: chr "css/crosstalk.min.css"
  .. ..$ head      : NULL
  .. ..$ attachment: NULL
  .. ..$ package   : chr "crosstalk"
  .. ..$ all_files : logi TRUE
  .. ..- attr(*, "class")= chr "html_dependency"
  ..$ :List of 10
  .. ..$ name      : chr "plotly-htmlwidgets-css"
  .. ..$ version   : chr "2.11.1"
  .. ..$ src       :List of 1
  .. .. ..$ file: chr "htmlwidgets/lib/plotlyjs"
  .. ..$ meta      : NULL
  .. ..$ script    : NULL
  .. ..$ stylesheet: chr "plotly-htmlwidgets.css"
  .. ..$ head      : NULL
  .. ..$ attachment: NULL
  .. ..$ package   : chr "plotly"
  .. ..$ all_files : logi FALSE
  .. ..- attr(*, "class")= chr "html_dependency"
  ..$ :List of 10
  .. ..$ name      : chr "plotly-main"
  .. ..$ version   : chr "2.11.1"
  .. ..$ src       :List of 1
  .. .. ..$ file: chr "htmlwidgets/lib/plotlyjs"
  .. ..$ meta      : NULL
  .. ..$ script    : chr "plotly-latest.min.js"
  .. ..$ stylesheet: NULL
  .. ..$ head      : NULL
  .. ..$ attachment: NULL
  .. ..$ package   : chr "plotly"
  .. ..$ all_files : logi FALSE
  .. ..- attr(*, "class")= chr "html_dependency"
 $ elementId    : NULL
 $ preRenderHook:function (p, registerFrames = TRUE)  
 $ jsHooks      : list()
 - attr(*, "class")= chr [1:2] "plotly" "htmlwidget"
 - attr(*, "package")= chr "plotly"
