import Text.Read

-- ask number, if number double
interactiveDoubling = do
  putStrLn "Number: "
  s <- getLine
  let mx = readMaybe s :: Maybe Double
  case mx of
    Just x -> putStrLn ("double is " ++ show (2*x))
    Nothing -> do
      putStrLn "A number I said!!"
      interactiveDoubling

-- ask number, if number double
-- using the fact that Maybe is a Functor
interactiveDoublingF = do
  putStrLn "Number: "
  s <- getLine
  let mx = readMaybe s :: Maybe Double
  case fmap (2*) mx of
    Just x -> putStrLn ("double is " ++ show x)
    Nothing -> do
      putStrLn "A number I said!!"
      interactiveDoubling

--somme de deux nombres
interactiveSum = do
  putStrLn "Nombres: "
  sx <- getLine
  sy <- getLine
  let mx = readMaybe sx :: Maybe Double  
      my = readMaybe sy :: Maybe Double
  case mx of
    Just x -> case my of
                Just y -> putStrLn ("sum is " ++ show (x+y))
                Nothing -> retry
    Nothing -> retry
    where retry = do
            putStrLn "Des nombres !!" 
            interactiveSum

--somme de deux nombres, Applicative
interactiveSumA = do
  putStrLn "Nombres: "
  sx <- getLine
  sy <- getLine
  let mx = readMaybe sx :: Maybe Double  
      my = readMaybe sy :: Maybe Double
  case fmap (+) mx <*> my of
    Just s -> putStrLn ("sum is " ++ show (s))
    Nothing -> do
      putStrLn "Des nombres !!" 
      interactiveSum
