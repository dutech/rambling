module Token where

-- ************************************************************************ Piece
data Color = Black | White | Green
  deriving (Eq,Ord)

instance Show Color where
  show Black = "B_"
  show White = "W_"
  show Green = "G_"

data Piece = Token Color Int
  deriving (Eq) -- Read ?
instance Show Piece where
  show (Token c x) = show c ++ show x
  
instance Ord Piece where
  Token c x <= Token d y =  (c <= d && x == y ) || x <= (y-1)

-- all the tokens
all_token:: [Piece]
all_token = [Token t x | t <- [Black, White], x <- [0..4]++[6..9]]++[Token Green 5,Token Green 5]

-- ************************************************************************* Code
type Code = [Piece]

-- ************************************************************************ Query
type Result = Int
type Query = Code -> Result

-- *************************************************************** Question Cards
-- ************************************************************************* Card
data Card  = Card
  { name :: String
  , fn   :: Query
  }
instance Show Card where
  show (Card n f) = show n
  
all_cards = [
  Card "NbW" (nb_color White),
  Card "NbB" (nb_color Black),
  Card "NbG" (nb_color Green)
  ]
  
-- ************************************************************** Inner functions
-- helper functions to halp define the Question Cards
-- position in hand of a number
-- number of Color
nb_color :: Color -> Query
nb_color _ [] = 0
nb_color col (Token c _ : ts)
  | c == col   = 1 + nb_color col ts
  | otherwise  = nb_color col ts

-- number of Odd/Even 
nb_odd :: Query
nb_odd [] = 0
nb_odd (Token _ x : ts)
  | odd x  = 1 + nb_odd ts
  | otherwise = nb_odd ts
nb_even :: Query
nb_even [] = 0
nb_even (Token _ x : ts)
  | even x  = 1 + nb_even ts
  | otherwise = nb_even ts

-- number of cards that follow
nb_follow :: Query
nb_follow l = in_seq l False
-- need to remember if we are already IN a sequence to properly count
-- the number of followers
in_seq :: [Piece] -> Bool -> Int 
in_seq (Token _ x : Token c y : ts ) seq
  | x+1  == y  && seq     = 1 + in_seq (Token c y : ts) True
  | x+1  == y  && not seq = 2 + in_seq (Token c y : ts) True
  | otherwise = in_seq (Token c y : ts) False
in_seq _ _ = 0

-- number of neighboring cards of same colors
nb_same_color :: [Piece] -> Int
nb_same_color l = in_color l False
-- need to remember if we are already IN a sequence to properly count
-- the number of followers
in_color :: [Piece] -> Bool -> Int 
in_color (Token c _ : Token d y : ts ) seq
  | c  == d  && seq     = 1 + in_color (Token d y : ts) True
  | c  == d  && not seq = 2 + in_color (Token d y : ts) True
  | otherwise = in_color (Token d y : ts) False
in_color _ _ = 0

-- sum of Color
sum_color :: Color -> Query
sum_color _ [] = 0
sum_color col (Token c x : ts)
  | c == col  = x + sum_color col ts
  | otherwise = sum_color col ts

-- sum of all cards
sum_all :: Query
sum_all l = sum $ map (flip sum_color l) [Black, White, Green]

-- BEURK
-- sum of some of the cards
sum_left :: Query
sum_left (Token _ x1 : Token _ x2 : Token _ x3 : Token _ x4 : Token _ x5 : ts) = x1+x2+x3
sum_left _ = 0

sum_right :: Query
sum_right (Token _ x1 : Token _ x2 : Token _ x3 : Token _ x4 : Token _ x5 : ts) = x3+x4+x5
sum_right _ = 0

sum_center :: Query
sum_center (Token _ x1 : Token _ x2 : Token _ x3 : Token _ x4 : Token _ x5 : ts) = x2+x3+x4
sum_center _ = 0

-- position of given Token value (or [] if not present)
pos :: Int -> [Piece] -> [Int]
pos x l = map ((length l) -) (poslen x l)

poslen :: Int -> [Piece] -> [Int]
poslen _ [] = []
poslen y (Token _ x : xs) 
  | x==y      = [length xs] ++ poslen y xs
  | otherwise = poslen y xs


