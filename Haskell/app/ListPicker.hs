module ListPicker where

import System.Random

-- extract 'nth' elem from list
-- extract n list will return (nth element, [leftovers])
extract :: Int -> [a] -> (a, [a])
extract n l = (x, xs ++ ys)
  where
    xs = take n l
    x:ys= drop n l

-- randomly choose (and extract) one element from list
-- choose list randGen will return ((chosen, [leftovers]), gen'')
choice :: RandomGen g => [a] -> g -> ((a, [a]), g)
choice l g = ((e, l'), snd rnd)
  where
    (e, l') = extract (fst rnd) l 
    len = length l
    rnd = uniformR (0,len-1) g 

-- randomly pick n elements from list
-- pick n list randGen will return (([picked elements], [leftovers]), gen'')
pick :: RandomGen g => Int -> [a] -> g -> (([a], [a]), g)
pick 0 l gen = (([], l), gen)
pick n l gen = ((e:es, lss), gen'')
  where
    ((e, ls), gen') = choice l gen
    ((es, lss), gen'') = pick (n-1) ls gen'

-- all possible subsequence of a list (++ append to the list )
subs :: [a] -> [[a]]
subs [] = [[]]
subs (x:xs) = yss ++ map (x:) yss
  where yss = subs xs

-- keep only lists of length n
keep :: [[a]] -> Int -> [[a]]
keep l n = filter ((==) n . length) l
