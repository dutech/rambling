module Main where

-- Playing with State (and StateMonad)
-- see https://en.wikibooks.org/wiki/Haskell/Understanding_monads/State

import Control.Monad
import System.Random

-- State and Output of transitions
data TurnstileState = Locked | Unlocked
  deriving (Eq, Show)

data TurnstileOutput = Thank | Open | Tut
  deriving (Eq, Show)

-- transition functions
coin, push :: TurnstileState -> (TurnstileOutput, TurnstileState)

coin _ = (Thank, Unlocked)

push Locked   = (Tut, Locked)
push Unlocked = (Open, Locked)

-- a sequence of steps
monday :: TurnstileState -> ([TurnstileOutput], TurnstileState)
monday s0 =
  let (a1, s1) = coin s0
      (a2, s2) = push s1
      (a3, s3) = push s2
      (a4, s4) = coin s3
      (a5, s5) = push s4
  in ([a1, a2, a3, a4, a5], s5)

-- some Person with behaviors
regularPerson, distractedPerson, hastyPerson :: TurnstileState -> ([TurnstileOutput], TurnstileState)

regularPerson s0 =
  let (a1, s1) = coin s0
      (a2, s2) = push s1
  in ([a1, a2], s2)

distractedPerson s0 =
  let (a1, s1) = coin s0
  in ([a1], s1)

hastyPerson s0 =
  let (a1, s1) = push s0
      (a2, s2) = coin s1
      (a3, s3) = push s2
  in if s1 == Unlocked
     then ([a1], s1)
     else ([a1, a2, a3], s3)  

tuesday :: TurnstileState -> ([TurnstileOutput], TurnstileState)
tuesday s0 =
  let (as1, s1) = regularPerson s0
      (as2, s2) = hastyPerson s1
      (as3, s3) = distractedPerson s2
      (as4, s4) = hastyPerson s3
  in ( as1 ++ as2 ++ as3 ++ as4, s4)

-- two people attempting to use the turnstile in succession. The first
-- is either a regularPerson or a distractedPerson (depending on the
-- Bool argument). The second person will simply push the arm without
-- inserting a coin and give up if they don't get through. The Bool
-- result should indicate whether the second person made it through.
luckyPair :: Bool -> TurnstileState -> (Bool, TurnstileState)
luckyPair False s0 =
    let (a1, s1) = regularPerson s0
        (a2, s2) = push s1
        res = (s2 == Unlocked)
  in (res, s2)
luckyPair True s0 =
    let (a1, s1) = distractedPerson s0
        (a2, s2) = push s1
        res = (s2 == Unlocked)
  in (res, s2)

-- turn all this into State (in fact, StateProcessor)
-- Besides, in the Haskell implementation, they use state :: (s->(s,a))->State s a
newtype State s a = State { runState :: s -> (a, s) }
state :: (s -> (a, s)) -> State s a
state = State
-- Make this a Monad instance
instance Functor (State s) where
  fmap = liftM

instance Applicative (State s) where
  pure = return
  (<*>) = ap

instance Monad (State s) where
-- return and bind
  --return :: a -> State s a
  return x = State (\s -> (x, s))

  --(>>=) :: State s a -> (a -> State s b) -> State s b
  p >>= k = q where
    rp = runState p        -- rp :: s -> (a, s)
    rk = runState . k      -- rk :: a -> s -> (b, s)
    qf s0 = (y, s2) where  -- qf :: s -> (b, s)
      (x, s1) = rp s0      -- (x, s1) :: (a, s)
      (y, s2) = rk x s1    -- (y, s2) :: (b, s)
    q = state qf           -- q :: State s b
-- alternativement
-- p >>= k = state $ \s0 ->
--   let (x, s1) = runState p s0  -- run the first stateProcessor on s0
--   in runstate (k x) s1         -- run the second stateProcessor on s1

-- get ONLY the RESULT of runing the StateProcessor
evalState :: State s a -> s -> a
evalState p s = fst (runState p s)

-- get ONLY the NEW STATE after runnin the StateProcessor
execState :: State s a -> s -> s
execState p s = snd (runState p s)

-- setting the state
put :: s -> State s ()
put newState = state $ \_ -> ((), newState)

-- get is also monadic and creates a state processor that gives back
-- the state s it is given both as a result and as the next
-- state. That means the state will remain unchanged, and that a copy
-- of it will be made available for us to use.
get :: State s s
get = state $ \s -> (s, s)
-- or gets f = fmap f get

-- which modifies the current state using a function
modify :: (s -> s) -> State s ()
modify f = state $ \ st -> ((), f st)

-- produces a modified copy of the state while leaving the state itself unchanged
gets :: (s -> a) -> State s a
gets f = state $ \ st -> (f st, st)

-- now can be expressed as States
coinS, pushS :: State TurnstileState TurnstileOutput
-- coinS = state coin
-- other definition below
-- pushS = state push

-- then mondayS
mondayS :: State TurnstileState [TurnstileOutput]
-- mondayS = do
--   a1 <- coinS
--   a2 <- pushS
--   a3 <- pushS
--   a4 <- coinS
--   a5 <- pushS
--   return [a1, a2, a3, a4, a5]
-- as sugar to >>= + lambda functions
-- mondayS =
--   coinS >>= ( \a1 ->
--     pushS >>= ( \a2 ->
--       pushS >>= ( \a3 ->        
--         coinS >>= ( \a4 ->
--           pushS >>= ( \a5 ->
--             return [a1, a2, a3, a4, a5] )))))
-- or even, using 'sequence' of actions
mondayS = sequence [coinS, pushS, pushS, coinS, pushS]

regularPersonS, distractedPersonS, hastyPersonS :: State TurnstileState [TurnstileOutput]
-- regularPersonS = sequence [coinS, pushS]
-- After definition of TurnstileInput
regularPersonS = mapM turnS [Coin, Push]

--distractedPersonS = sequence [coinS]
distractedPersonS = mapM turnS [Coin]

-- hastyPersonS = do 
--   a1 <- pushS
--   if a1 == Open
--     then do return [a1]
--     else do
--       ax <- sequence [ coinS, pushS ]
--       return (a1:ax)
hastyPersonS = do
  a1 <- turnS Push
  if a1 == Open
    then do return [a1]
    else do
      ax <- mapM turnS [Coin, Push]
      return (a1:ax)

    
luckyPairS :: Bool -> State TurnstileState Bool
luckyPairS isFirstDistracted = do
  if isFirstDistracted
    then distractedPersonS
    else regularPersonS
  a2 <- pushS
  return (a2 == Open)

-- using put, we can SEt the state inside "do" statement
-- example : function to test our Turnstile
testTurnstile :: State TurnstileState Bool
testTurnstile = do
  s_ori <- get
  -- checking locking...
  put Locked
  check1 <- pushS
  put Unlocked
  check2 <- pushS

  -- checking coins
  put Locked
  coinS
  check_s1 <- get
  put Unlocked
  coinS
  check_s2 <- get
  -- return to original state
  put s_ori
  return (check1 == Tut && check2 == Open && check_s1 == Unlocked && check_s2 == Unlocked)

-- pushS can be written without the push :: TurnstileState -> (TurnstileOutput, TurnstileState) function
-- pushS = state $ \s -> case s of
--   Locked -> (Tut, Locked)
--   Unlocked -> (Open, Locked)
pushS = do
  s <- get
  put Locked -- ensure new State is Locked
  case s of  -- and prepare output
    Locked -> return Tut
    Unlocked -> return Open

coinS = do
  put Unlocked
  return Thank

-- Focussing on Input
data TurnstileInput = Coin | Push
  deriving (Eq, Show)

turnS :: TurnstileInput -> State TurnstileState TurnstileOutput
turnS = state . turn where
  turn Coin _        = (Thank, Unlocked)
  turn Push Unlocked = (Open, Locked)
  turn Push Locked   = (Tut,  Locked)

tuesdayS :: State TurnstileState [TurnstileOutput]
tuesdayS = do
  ax <- sequence [regularPersonS, hastyPersonS, distractedPersonS, hastyPersonS]
  return (concat ax)

-- implement saveCoinsS :: [TurnstileInput] -> State TurnstileState
-- Int that potentially processes all of the given inputs, but will
-- skip a Coin input if the previous input generated a Thanks. It
-- returns the number of Coin inputs that were skipped. E.g. evalState
-- (saveCoinsS [Push, Coin, Coin, Coin, Push, Push, Coin, Push])
-- Locked should give 2.
saveCoinsS :: [TurnstileInput] -> State TurnstileState Int
saveCoinsS inputs = do
  (_, n) <- foldM maybeTurn (Nothing, 0) inputs
  return n
  where maybeTurn (Just Thank, n) Coin = return (Just Thank, n+1)
        maybeTurn (_,     n) inp  = do out <- turnS inp; return (Just out, n)

-- implement sequenceUntil :: (a -> Bool) -> [State s a] -> State s
-- [a]. It processes each of the inputs until one of them generates a
-- value that matches the predicate, then processes no
-- more. E.g. evalState (sequenceUntil (== Open) [coinS, coinS, coinS,
-- pushS, pushS, coinS]) Locked should give [Thank,Thank,Thank,Open]
--sequenceUntil :: (a -> Bool) -> [State s a] -> State s [a]
-- Modify sequenceUntil so that it works with any Monad instance.
sequenceUntil :: Monad m => (a -> Bool) -> [m a] -> m [a]
sequenceUntil test [] = return []
sequenceUntil test (x:xs) = do
  aa <- x
  if test aa
    then return [aa]
    else do
      as <- sequenceUntil test xs
      return (aa:as)

-- Playing with random
rollPair :: StdGen -> ((Int, Int), StdGen)
rollPair s0 =
  let (d1,s1) = randomR (1,6) s0
      (d2,s2) = randomR (1,6) s1
  in ((d1,d2), s2)

-- implement rollSix that returns a list representing the result of six consecutive throws.
rollSix :: StdGen -> ([Int], StdGen)
rollSix s0 =
  let (d1,s1) = randomR (1,6) s0
      (d2,s2) = randomR (1,6) s1
      (d3,s3) = randomR (1,6) s2
      (d4,s4) = randomR (1,6) s3
      (d5,s5) = randomR (1,6) s4
      (d6,s6) = randomR (1,6) s5
  in ([d1,d2,d3,d4,d5,d6], s6)


rollN :: Int -> StdGen -> ([Int], StdGen)
rollN n g =
  -- apply randomR (1,6) to the snd argument of (randomR (1,6) s0)
  let xs = take n (iterate (randomR (1,6) . snd) (randomR (1,6) g))
  in (map fst xs, snd $ last xs)

-- rollDIce with State
rollDieS :: State StdGen Int
rollDieS = state (randomR (1,6))

rollSixS :: State StdGen [Int]
rollSixS = do
  d1 <- rollDieS
  d2 <- rollDieS
  d3 <- rollDieS
  d4 <- rollDieS
  d5 <- rollDieS
  d6 <- rollDieS
  return [d1, d2, d3, d4, d5, d6]

rollNS :: Int -> State StdGen [Int]
rollNS n = replicateM n rollDieS

luckyDoubleS :: State StdGen Int
luckyDoubleS = do
  d1 <- rollDieS
  if d1 == 6
    then
      do
        d2 <- rollDieS
        return (d1+d2)
    else
      return d1

main :: IO ()
main = do
  print $ "monday Locked" ++ show (monday Locked)
  print $ "tuesday Locked" ++ show (monday Locked)
  print $ "luckyPair True" ++ show (luckyPair True Locked)
  print $ "luckyPair False" ++ show (luckyPair False Locked)

  print $ "__ Now using States *************************************************"
  print $ "runState coinS = " ++ show (runState coinS Locked)
  print $ "runState mondayS = " ++ show (runState mondayS Locked)
  print $ "runstate luckyPairS True" ++ show (runState (luckyPairS True ) Locked)
  print $ "runstate testTurnstile " ++ show (runState testTurnstile Locked)
  print $ "evalstate (replicateM) " ++ show (evalState (replicateM 6 pushS) Unlocked)
  

  print $ "__ As a Transducer **************************************************"
  print $ "mapM turnS " ++ show (evalState (mapM turnS [Coin, Push, Push, Coin, Push]) Locked)
  print $ "map tuesdayS " ++ show( runState tuesdayS Locked)
  print $ "saveCoins " ++ show (evalState (saveCoinsS [Push, Coin, Coin, Coin, Push, Push, Coin, Push]) Locked)
  print $ "sequenceUntil " ++ show (evalState (sequenceUntil (== Open) [coinS, coinS, coinS, pushS, pushS, coinS]) Locked)

  print $ "__ Using Random *****************************************************"
  print $ "rollPair = " ++ show (rollPair (mkStdGen 666))
  print $ "rollSix = " ++ show (rollSix (mkStdGen 666))  
  print $ "rollN 10 = " ++ show( rollN 10 (mkStdGen 666))
  print $ "rollDieS = " ++ show ( evalState rollDieS (mkStdGen 666) )
  print $ "rollNS 10 =" ++ show ( evalState (rollNS 10) (mkStdGen 666))
  print $ "rollNS 10 =" ++ show ( evalState luckyDoubleS (mkStdGen 666))
