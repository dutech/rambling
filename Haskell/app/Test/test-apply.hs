module Main where

import ListPicker
import Token
import Data.List
import System.Random

import Prelude hiding (lookup)
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.Maybe (fromMaybe)

-- Apply a Card to all [Piece] (i.e. Hand/Code) of the [[Piece]]
-- Usually, we get an Int
-- => should be nice to get a list of all the Hand/Code that return the same
--    result, as [ (Int, [[Piece]]) ]
-- listResults card [codes] will return [ (result, [the code with this result])
-- (TODO cela aurait pu être un fmap)
listResults :: Query -> [Code] -> [(Result, [Code])]
listResults _ [] = []
listResults card (x:xs) = [(n, [x])] ++ (listResults card xs)
  where n = card x

-- Apply a Card to all Code of the list of Code
-- Map is [(event, nb_occurence)]
-- as we get Result=Int, lets build a Data.Map Int -> nb_of_results
-- by repeatidly modifying the map whith result of applying a card
countResults :: Query -> [Code] -> IntMap Int -> IntMap Int
countResults card options m0 = foldr step m0 options
  where
    step hand m = updateMapFromQuery card hand m 

-- Function that take a Card.fn, a Code and a Map and returns
-- a result (Int) and a modified Map
-- The Map is (answer_to_Query, nb_of( Code that give this answer) )
--updateMapFromQuery :: ([Piece] -> Int) -> [Piece] -> IntMap Int -> IntMap Int
updateMapFromQuery :: Query -> Code -> IntMap Int -> IntMap Int
updateMapFromQuery card hand m = IntMap.insert res (nb+1) m
  where
    res = card hand
    -- 0 or the the value of the map associated to res
    nb = fromMaybe 0 (IntMap.lookup res m)

-- from a list of (result, nb_of _results)
-- gather (sum of (results x nb_of_results), sum of nb_of_results)
expectation :: [(Int, Int)] -> (Int, Int)
expectation density = foldr step (0,0) density
  where
    step (x,y) (xx,yy) = (xx+x*y, y+yy)
-- Given the expectation (sum p*x, sum p)  average is sum p*x / sum p  
average :: (Int,Int) -> Float
average (x,y) = xf / yf
  where xf = fromIntegral x :: Float
        yf = fromIntegral y :: Float


-- List of frequence of results to Probabilities
-- from a list of (result, nb_of _results) will return a list of [nb_of_res/sum]
type Density = [Float]
toDensity :: [(Int, Int)] -> Density
toDensity freq = map to_proba freq
  where
    nb_total = fromIntegral (foldr step  0 freq) :: Float
    step (xs, ys) r = ys + r
    to_proba (x,y) = let yf = fromIntegral y ::Float in yf / nb_total

-- Entropy of density, where density is a list of proba
entropy :: [Float] -> Float
entropy density = foldr step 0 density
  where step p e = e + p * log p

-- Eval a card => score is the entropy of the density of results
-- Even if Density of card do not always have the same lenght (some cards have
-- fewer resuls), these 0 proba contribute to entropy with a score of 0*log(0)
-- that is therefore evaluated as 0.
type Entropy = (Float, Density)
evalCardEntropy :: Query -> [Code] -> Entropy
evalCardEntropy card combinations = (entropy $ dens, dens)
  where freqRes = IntMap.toAscList (countResults card combinations IntMap.empty)
        dens = toDensity freqRes
-- Eval Entropy of a list of cards
evalAllEntropy :: [Card] -> [Code] -> [(Entropy, Card)]
evalAllEntropy l_cards combinations = map step l_cards
  where step (Card n fn) = (evalCardEntropy fn combinations, (Card n fn)) 


-- Compute the average of events given a Density as a list of Proba
-- and a list (same Length) of (event, nb_of_events)
-- TODO not very efficient, see 'average above'
avgLength :: Density -> [(Int, Int)] -> Float
avgLength dens l_length = avg
  where weighted_length = zipWith (\p (l, _) -> let lf = fromIntegral l :: Float in p*lf) dens l_length
        avg = sum weighted_length

-- Eval average length of the answers of a Card
type Average = (Float, [Int], Density)
evalCardAvg :: Query -> [Code] -> Average
evalCardAvg card combinations = (avgLength dens freq_res, lengths, dens)
  where freq_res = IntMap.toAscList (countResults card combinations IntMap.empty)
        dens = toDensity freq_res
        lengths = [l | (l,_) <- freq_res]

-- Eval average length of a list of cards
evalAllAvg :: [Card] -> [Code] -> [(Average, Card)]
evalAllAvg l_cards combinations = map step l_cards
  where step (Card n fn) = (evalCardAvg fn combinations, (Card n fn)) 


-- Example of some Card.function's (Query)
cards = [nb_color Black, nb_color White, nb_color Green]
            
main :: IO ()
main = do
  -- Random Generator
  gen <- newStdGen
  -- Create mhand, a Hand of 5 Piece
  let took = pick 5 all_token gen 
  let mhand = sort $ fst $ fst $ took
  -- leftover tokens after chosing 5 making mhand
  let leftover = snd $ fst $ took

  -- From this, build all possible hands
  -- all possible options using 'left' tokens
  let options = map sort (keep (subs leftover) 5)
  -- first 10 possibles combinations
  --let combinations = take 10 options
  let rnd_comb = pick 10 options gen
  let combinations = fst $ fst $ rnd_comb

  print $ "mhand= " ++ show mhand  
  print $ "comb= " ++ show combinations

  putStrLn "\n__LIST RESULTS of 'nb_color Black' on combinations ***************"
  let resB = listResults (nb_color Black) combinations
  putStrLn $ "resB=" ++ show resB

  putStrLn "\n__COUNT RESULT of nb_color Black on combinations *****************"
  let empty = IntMap.empty
  let resM = countResults (nb_color Black) combinations empty
  print $ "resM=" ++ show resM
  print $ "toAsclist=" ++ (show (IntMap.toAscList resM))
  putStrLn "__EXPECTATION of results of applying this card"
  let expect = expectation (IntMap.toAscList resM)
  print $ "E = " ++ show expect ++ "  [intemediary result]"
  print $ "E= " ++ show (average expect)

  putStrLn "\n__DENSITY and ENTROPY ********************************************"
  let density = toDensity (IntMap.toAscList resM)
  print $ "P= " ++ show density
  print $ "Ent= " ++ show (entropy density)

  putStrLn "\n__ENTROPY of a CARD **********************************************"
  let eval_nbB = evalCardEntropy (nb_color Black) combinations
  print $ "Ent_nbB= " ++ show eval_nbB
  let eval_nbW = evalCardEntropy (nb_color White) combinations
  print $ "Ent_nbW= " ++ show eval_nbW  
  let eval_nbG = evalCardEntropy (nb_color Green) combinations
  print $ "Ent_nbG= " ++ show eval_nbG

  putStrLn "\n__ENTROPY of a list of CARDS ************************************"
  putStrLn "((entropy,[density], Card)"
  let evals = evalAllEntropy all_cards combinations
  --print $ "Eval= " ++ show (map fst evals)
  print $ "Eval= " ++ show (evals)

  putStrLn "\n__AVERAGE LENGTH of results for a list of CARDS *****************"
  putStrLn "((avgLength, [events], [freq events]), Card)"
  let evals_a = evalAllAvg all_cards combinations
  --print $ "Eval= " ++ show (map fst evals)
  print $ "EvalAVG= " ++ show (evals_a)
