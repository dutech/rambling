module Main where

import ListPicker
import Token
import Data.List
import System.Random

li = [1,2,3,4,5]
sli = subs li


-- all possible subsequence of a list (++ append to the list )
subs :: [a] -> [[a]]
subs [] = [[]]
subs (x:xs) = yss ++ map (x:) yss
  where yss = subs xs

-- keep only lists of length n
keep :: [[a]] -> Int -> [[a]]
keep l n = filter ((==) n . length) l

cards = [nb_color Black, nb_color White, nb_color Green]


-- Apply card (i.e. function) to a hand and a list of options and return the result
apply_card :: ([Piece] -> Int) -> [Piece] -> [[Piece]] -> [[Piece]]
apply_card fun hand options = filter same options
  where res = fun hand
        same = (==) res . fun

-- Estimate the utility of playing a Card
-- Many results are possible
-- => How do we assess the usefullness of playing it ?


main :: IO ()
main = do
  gen <- newStdGen
  let took = pick 5 all_token gen
  let mhand = sort $ fst $ fst $ took
  let left = snd $ fst $ took
  let ohand = sort $ fst $ fst $ pick 5 left gen

  print $ "mhand= " ++ show mhand
  let options = map sort (keep (subs left) 5)
  print $ "left= " ++ show(length( options ))
  print $ "other=" ++ show ohand

  let o_nb_white = nb_color White ohand
  print $ "     nb_white = " ++ show o_nb_white
  let reducedW = filter same options
        where same = (==) o_nb_white . nb_color White
  print $ "reduced= " ++ show( length reducedW ) ++ show( head reducedW)

  let o_nb_black = nb_color Black ohand
  print $ "     nb_black = " ++ show o_nb_black
  let reducedB = filter same options
        where same = (==) o_nb_black . nb_color Black
  print $ "reduced= " ++ show( length reducedB ) ++ show( head reducedB)

  let o_nb_green = nb_color Green ohand
  print $ "     nb_green = " ++ show o_nb_green
  let reducedG = filter same options
        where same = (==) o_nb_green . nb_color Green
  print $ "reduced= " ++ show( length reducedG ) ++ show( head reducedG)

  print $ "     nb_green = " ++ show (nb_color Green ohand)
  print $ "reduced= " ++ show( length (apply_card (nb_color Green) ohand options))
  
  print $ "     nb_odd = " ++ show (nb_odd ohand)
  print $ "reduced= " ++ show( length (apply_card (nb_odd) ohand options))

  print $ "     nb_even = " ++ show (nb_even ohand)
  print $ "reduced= " ++ show( length (apply_card (nb_even) ohand options))

  print $ "     nb_follow = " ++ show (nb_follow ohand)
  print $ "reduced= " ++ show( length (apply_card (nb_follow) ohand options))

  print $ "     nb_same_color = " ++ show (nb_same_color ohand)
  print $ "reduced= " ++ show( length (apply_card (nb_same_color) ohand options))

  print $ "     sum_black = " ++ show (sum_color Black ohand)
  print $ "reduced= " ++ show( length (apply_card (sum_color Black) ohand options))

  print $ "     sum_all = " ++ show (sum_all ohand)
  print $ "reduced= " ++ show( length (apply_card (sum_all) ohand options))

  print $ "     sum_left = " ++ show (sum_left ohand)
  print $ "reduced= " ++ show( length (apply_card (sum_left) ohand options))
