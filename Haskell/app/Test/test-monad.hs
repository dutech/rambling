module Main where

-- playing with Monad
-- from https://en.wikibooks.org/wiki/Haskell/Understanding_monads#LiftM_and_Friends
-- and https://en.wikibooks.org/wiki/Haskell/Prologue:_IO,_an_applicative_functor



-- Monad defined by
-- * type constructor m
-- * function return :: a -> m a
-- * operartor >>= which binds :: m a -> (a -> m b) -> m b

p >>= 

-- ******************************************************************************
main :: IO ()
main = do
  print $ "__Test Monad ********************************************************"
