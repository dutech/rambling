module Main where

import Token
import ListPicker
import Data.List
import System.Random

main :: IO ()
main = do
  print $ Token Black 3
  print $ "--- All Token"
  print $ sort all_token
  gen <- newStdGen

  -- hand is a sorted list of 5 Piece randomly taken from all_tokens
  let hand = sort $ fst $ fst $ pick 5 all_token gen
  --let hand = [Token Green 5, Token Black 7, Token White 7, Token Black 8, Token White 9]

  print $ hand
  print $ "nb_black = " ++ show (nb_color Black hand)
  -- print $ "nb_black  = " ++ show (nb_black hand)
  print $ "nb_white = " ++ show (nb_color White hand)
  print $ "nb_green = " ++ show (nb_color Green hand)
  print $ "nb_odd   = " ++ show (nb_odd hand)
  print $ "nb_even   = " ++ show (nb_even hand)
  print $ "sum_black = " ++ show (sum_color Black hand)
  print $ "sum_white = " ++ show (sum_color White hand)
  print $ "sum_green = " ++ show (sum_color Green hand)
  print $ "sum_all   = " ++ show (sum_all hand)  
  -- print $ "posL 3    = " ++ show (poslen hand 3)
  -- print $ "posL 5    = " ++ show (poslen hand 5)
  print $ "posL 7    = " ++ show (poslen 7 hand)
  print $ "pos 7     = " ++ show (pos 7 hand)
  print $ "pos 8     = " ++ show (pos 8 hand)
  print $ "seq       = " ++ show (in_seq hand False)
  print $ "seq       = " ++ show (nb_follow hand)
  print $ "col       = " ++ show (in_color hand False)
  print $ "col       = " ++ show (nb_same_color hand)
  print $ "sum_L     = " ++ show (sum_left hand)
  print $ "sum_C     = " ++ show (sum_center hand)
  print $ "sum_R     = " ++ show (sum_right hand)
