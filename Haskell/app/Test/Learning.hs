module Learning where
import Control.Monad
-- import Control.Monad.State

-- Haskell Wikibooks

-- Understanding Monads/Maybe

safeLog :: (Floating a, Ord a) =>a -> Maybe a
safeLog x
  | x > 0     = Just (log x)
  | otherwise = Nothing

  
phonebook :: [(String, String)]
phonebook = [ ("Bob",   "01788 665242"),
              ("Fred",  "01624 556442"),
              ("Alice", "01889 985333"),
              ("Jane",  "01732 187565") ]

-- Understanding Monads/List
-- Implement themselvesTimes :: [Int] -> [Int], which takes each number n {\displaystyle n} n in the argument list and generates n {\displaystyle n} n copies of it in the result list.

themselvesTimes :: [Int] -> [Int]
-- themselvesTimes l = l >>= (\x -> replicate x x)
-- concatMap f xs = concat (map f xs), concatenate the application of f
themselvesTimes = concatMap (\x -> replicate x x)

-- all Monads also have an instance of Applicative. In particular, (<*>) for that instance might be defined as:
-- fs <*> xs = concatMap (\f -> map f xs) fs
-- Write an alternative definition of (<*>) using a list comprehension. Do not use map, concat or concatMap explicitly.
alt_star fs xs = [ f x | f <- fs, x <- xs ]

-- State ***********************************************************************
data TurnstileState = Locked | Unlocked
  deriving (Eq, Show)

data TurnstileOutput = Thank | Open | Tut
  deriving (Eq, Show)

-- Input as functions
coin, push :: TurnstileState -> (TurnstileOutput, TurnstileState)

coin _ = (Thank, Unlocked)
push Locked = (Tut, Locked)
push Unlocked = (Open, Locked)

monday :: TurnstileState -> ([TurnstileOutput], TurnstileState)
monday s0 =
  let (a1, s1) = coin s0
      (a2, s2) = push s1
      (a3, s3) = push s2
      (a4, s4) = coin s3
      (a5, s5) = push s4
  in ([a1, a2, a3, a4, a5], s5)

regularPerson, distractedPerson, hastyPerson :: TurnstileState -> ([TurnstileOutput], TurnstileState)
regularPerson s0 =
  let (a1, s1) = coin s0
      (a2, s2) = push s1
  in ([a1, a2], s2)

distractedPerson s0 =
  let (a1,s1) = coin s0
  in ([a1], s1)
  
hastyPerson s0 =
  let (a1, s1) = coin s0
  in if a1 == Open
     then ([a1], s1)
     else
       let (a2, s2 ) = coin s1
           (a3, s3) = push s2
       in ([a1,a2,a3], s3)


tuesday :: TurnstileState -> ([TurnstileOutput], TurnstileState)
tuesday s0 =
  let (ax1, s1) = regularPerson s0
      (ax2, s2) = hastyPerson s1
      (ax3, s3) = distractedPerson s2
      (ax4, s4) = hastyPerson s3
  in (ax1 ++ ax2 ++ ax3 ++ ax4, s4)

luckyPair :: Bool -> TurnstileState -> (Bool, TurnstileState)
luckyPair firstDistracted s0 =
  let (_, s1) = if firstDistracted
        then distractedPerson s0
        else regularPerson s0
      (a2, s2) = push s1
  in (a2==Open, s2)

-- State using newtype (type with ONE constructor and ONE field)
-- it is rather a StateProcessor than a State
-- in Control.Monad.State, a state function is exported
-- state :: (s -> (a, s)) -> State s a
newtype State s a = State { runState :: s -> (a, s) }
--state :: (s -> (a, s)) -> State s a
--state = State

instance Functor (State s) where
  fmap f s0 = State (\x -> let (a1, s1) = runState s0 x in (f a1, s1))

instance Applicative (State s) where
  pure x = State (\s -> (x, s))
  p <*> q = State (\s0 -> let (a1,s1) = runState q s0
                              (f,s2) = runState p s1
                          in (f a1, s2))

instance Monad (State s) where
  return x = State (\s -> (x, s))
  p >>= q = State (\s0 -> let (a1,s1) = runState p s0
                              in runState (q a1) s1)

get :: State s s
get = State( \s -> (s,s))

put :: s -> State s ()
put s = State( \_ -> ((), s))

evalState :: State s a -> s -> a
evalState p s = fst (runState p s)

execState :: State s a -> s -> s
execState p s = snd (runState p s)

-- modifies the current state using a function
modify :: (s -> s) -> State s ()
modify f = State ( \s -> ((), f s))
-- produces a modified copy of the state while leaving the state itself unchanged
gets :: (s -> a) -> State s a
gets f = State( \s -> (f s, s))

coinS, pushS :: State TurnstileState TurnstileOutput
coinS = State coin
pushS = State push

data OutF = Merci | Ouvert | Grrrr
  deriving (Eq, Show)

toF :: TurnstileOutput -> OutF
toF Thank = Merci
toF Open = Ouvert
toF Tut = Grrrr

-- specfmap :: (TurnstileOutput -> OutF) -> State ts TurnstileOutput -> State ts OutF
-- specfmap f s0 = State (\x -> let (a1, s1) = runState s0 x in (f a1, s1))

specpure :: a -> State s a --TurnstileOutput -> State TurnstileState TurnstileOutput
specpure x = State (\s -> (x, s))

specreturn :: a -> State s a
specreturn x = State (\s -> (x, s))

specbind :: State s a -> (a -> State s b) -> State s b
specbind s0 toSb = State (\x -> let (a1, s1) = runState s0 x
                                    in runState (toSb a1) s1)


mondayS :: State TurnstileState [TurnstileOutput]
mondayS = do
  a1 <- coinS
  a2 <- pushS
  a3 <- pushS
  a4 <- coinS
  a5 <- pushS
  return [a1, a2, a3, a4, a5]

-- Extend testTurnstile so that it also checks the state is set to Unlocked after a coin is inserted, regardless of the state beforehand. And for good measure, have testTurnstile return the turnstile to it's original state when the testing is complete.
testTurnstile :: State TurnstileState Bool
testTurnstile = do
  initState <- get
  put Locked
  check1 <- pushS
  put Unlocked
  check2 <- pushS
  put Locked
  check3 <- coinS
  put Unlocked
  check4 <- coinS
  put initState
  return (check1 == Tut && check2 == Open && check3 == Open && check4 == Open)

data TurnstileInput = Coin | Push
  deriving (Eq, Show)
  
turnS :: TurnstileInput -> State TurnstileState TurnstileOutput
turnS = State . turn where
  turn Coin _        = (Thank, Unlocked)
  turn Push Unlocked = (Open,  Locked)
  turn Push Locked   = (Tut,   Locked)
  
