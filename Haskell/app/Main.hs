module Main where

import HaskellSay (haskellSay)
import System.Random
import Data.List
import Token 

-- extract 'nth' elem from list
extract :: Int -> [a] -> (a, [a])
extract n l = (x, xs ++ ys)
  where
    xs = take n l
    x:ys= drop n l

-- extract 'n' randomly selected elements from list, no replacement
-- pick :: Int -> [Int] -> [a] -> ([a], [a])
-- pick 0 _ l = ([], l)
-- pick n (r:rs) l = (e':es, ls)
--   where 
--     (e',l') = extract r l
--     (es,ls) = pick (n-1) rs l'

choice :: RandomGen g => [a] -> g -> ((a, [a]), g)
choice l g = ((e, l'), snd rnd)
  where
    (e, l') = extract (fst rnd) l 
    len = length l
    rnd = uniformR (0,len-1) g 

pick :: RandomGen g => Int -> [a] -> g -> (([a], [a]), g)
pick 0 l gen = (([], l), gen)
pick n l gen = ((e:es, lss), gen'')
  where
    ((e, ls), gen') = choice l gen
    ((es, lss), gen'') = pick (n-1) ls gen'


li = [1,2,3,4,5]

main :: IO ()
main = do
  gen <- newStdGen
  let ns = randoms gen :: [Int]
  print $ take 10 ns
  -- putStrLn "Hello, Haskell!"
  haskellSay "Hello Haskell user...."
  print $ Token Black 4
  print $ head all_cards
