module Main where
{-
  MonadPlus (or rather, MonadZero) as a failure/success path => BreadthFirst Search
  Taken from
  https://blog.jle.im/entries/series/+monadplus-success-failure-monads
-}
import Control.Monad ( MonadPlus, guard, mzero )

-- definition of 'halve' with only MonadPlus elements
halve :: Int -> Maybe Int
halve n = guard (even n)
          >> return (div n 2)

-- definition of 'halve' with only MonadPlus elements for List
halveL :: Int -> [Int]
halveL n = guard (even n)
          >> return (div n 2)

-- definition of 'halveGeneric' with only MonadPlus elements
halveGeneric :: (MonadPlus m, Integral a) => a -> m a
halveGeneric n = guard (even n)
          >> return (div n 2)

-- List as MonadPlus => all the path that leads to success
-- i.e. : in do blocks, Lists and Maybes are structurally identical.
halveOrDouble :: Integral a => a -> [a]
halveOrDouble n
  | even n    = [div n 2, n * 2]
  | otherwise = [n * 2]
-- using a 'do' notation for a single path
hod2PlusOne :: Integral a => a -> [a]
hod2PlusOne n = do
  -- REMEMBER : if path fail, il fails to the end => x success can lead to return FAILURE
  x <- halveOrDouble n
  halveOrDouble x
  return (x+1)
-- same thing using >>=
hod2PlusOneExpr n = halveOrDouble n
                    >>= (\x -> halveOrDouble x
                         >> return (x+1))

-- triangle de Pythagore
tripletUnder :: (Num c, Eq c, Enum c) => c -> [(c, c, c)]
tripletUnder n = do
  a <- [1..n]
  b <- [a..n]
  c <- [b..n]
  guard $ a^2 + b^2 == c^2
  return (a,b,c)
{-
A farmer has a wolf, a goat, and a cabbage that he wishes to transport across a river.
Unfortunately, his boat can carry only one thing at a time with him. He can’t leave
the wolf alone with the goat, or the wolf will eat the goat. He can’t leave the goat
alone with the cabbage, or the goat will eat the cabbage. How can he properly
transport his belongings to the other side one at a time, without any disasters?

So the plan is:

 1. Start with a blank plan; a tabula rasa.
 2. Add a legal and safe move to it.
 3. Repeat Step 2 n times
 4. Fail if you aren’t a solution; succeed if you are.
-}

data Character = Farmer | Wolf | Goat | Cabbage
  deriving (Show, Eq, Enum)

newtype Move = MoveThe Character
  deriving (Eq)
instance Show Move where
  show (MoveThe Farmer)  = "F"
  show (MoveThe Wolf)    = "W"
  show (MoveThe Goat)    = "G"
  show (MoveThe Cabbage) = "C"

type Plan = [Move]

data Position = West | East
  deriving (Show, Eq)

-- Plan and Move
startingPlan :: Plan
startingPlan = []

-- take a Plan and produce all successful way you can add a move
makeMove :: Plan -> [Plan]
makeMove p = do
  next <- fmap MoveThe [Farmer ..] -- apply MoveThe inside the [] Functor, and "pick" one
  guard $ moveLegal p next
  guard . not $ moveRedundant p next
  let newPlan = p ++ [next]
  guard $ safePlan newPlan
  return newPlan

moveLegal :: Plan -> Move -> Bool
moveLegal p (MoveThe Farmer) = True
moveLegal p (MoveThe c)      = positionOf p c == positionOf p Farmer

moveRedundant :: Plan -> Move -> Bool
moveRedundant [] m = False
moveRedundant p m  = last p == m

safePlan :: Plan -> Bool
safePlan p = goatPos == farmerPos || safeGoat && safeCabbage
  where
    goatPos = positionOf p Goat
    farmerPos = positionOf p Farmer
    safeGoat = goatPos /= positionOf p Wolf
    safeCabbage = positionOf p Cabbage /= goatPos

makeNMoves :: Int -> [Plan]
{-
  makeNMove n = makeMove startingPlan >>= makeMove >>= makeMove ... >>= makeMove
  iterate repeatedly apply a function
  iterate :: (a -> a) -> a -> [a]
  iterate f x = [x, f x, f (f x), f (f (f x)), ...]
-}
makeNMoves n = iterate (>>= makeMove) (return startingPlan) !! n

isSolution :: Plan -> Bool
isSolution p = all (== East) (map (positionOf p) [Farmer ..])

positionOf :: Plan -> Character -> Position
-- if even nb of Moves, then on bank East
-- the Farmer moves each time, Character are to be filtered
positionOf p c = case c of
  Farmer -> posFromCount . length $ p -- (posFromCount . length) p
  c      -> posFromCount . length $ filter (== MoveThe c) p
  where
    posFromCount n | even n    = West
                   | otherwise = East

findSolution :: Int -> [Plan]
findSolution n = do
  p <- makeNMoves n
  guard $ isSolution p
  return p
-- equivalent to
  -- makeNMoves n >>= (\p -> guard ( isSolution p )
  --                   >> return p)

-- *****************************************************************************
main :: IO ()
main = do
  print $ "halve 32 : " ++ show (halve 32)
  print $ "halve 32 >>= halve : " ++ show (halve 32 >>= halve)
  print $ "halve 32 >>= halve >>= halve : " ++ show (halve 32 >>= halve >>= halve)
  print $ "halve 32 >> mzero >>= halve : " ++ show (halve 32 >> mzero >>= halve)

  print ""
  print $ "halveL 32 : " ++ show (halveL 32)
  print $ "halveL 32 >>= halveL : " ++ show (halveL 32 >>= halveL)
  print $ "halveL 32 >>= halveL >>= halveL : " ++ show (halveL 32 >>= halveL >>= halveL)
  print $ "halveL 32 >> mzero >>= halveL : " ++ show (halveL 32 >> mzero >>= halveL)

  print ""
  print $ "halveGeneric 8 :: Maybe Int : " ++ show (halveGeneric 8 :: Maybe Int)
  print $ "halveGeneric 8 :: [Int] : " ++ show (halveGeneric 8 :: [Int])

  print ""
  print $ "halveOrDouble 6 :" ++ show (halveOrDouble 6)
  print $ "halveOrDouble 7 :" ++ show (halveOrDouble 7)
  print $ "halveOrDouble 6 >>= halveOrDouble :" ++ show (halveOrDouble 6 >>= halveOrDouble)
  print $ "halveOrDouble 7 >>= halveOrDouble :" ++ show (halveOrDouble 7 >>= halveOrDouble)
  print $ "hod2PlusOne 6 :" ++ show (hod2PlusOne 6)
  print $ "hod2PlusOneExpr 6 :" ++ show (hod2PlusOne 6)

  print ""
  print $ "tripletUnder 10 : " ++ show (tripletUnder 10)

  print ""
  print "__ Farmer and his boat"
  print $ "MakeNMoves 3 " ++ show (makeNMoves 3)
  print $ "findSolution 3 " ++ show (findSolution 3)
  print $ "findSolution 7 " ++ show (findSolution 7)
  print $ "findSolution 9 " ++ show (findSolution 9)
  print $ "findSolution 13 " ++ show (findSolution 13)
