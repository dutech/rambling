#!/usr/bin/env python3

"""
Calcul de distances entre deux "chaines de caractères",
mais aussi entre 2 séquences de symboles
"""

def hamming(seq1, seq2):

    if len(seq1) != len(seq2):
        return -1

    dist = 0
    for i in range(len(seq1)):
        if seq1[i] != seq2[i]:
            dist += 1

    return dist


def levenshtein(seq1, seq2):
    """Distance de Levenshtein.
    https://en.wikipedia.org/wiki/Levenshtein_distance

    Params:
    - seq1, seq2 : [char]

    Returns:
    - int : nb minimam d'édition OU insertion OU substitution.
    """
    m = len(seq1)
    n = len(seq2)

    # 2 temporary vector of integer distance
    # v0 (the previous row of distance) is the the edit distance from an empty seq1
    #    to seq2 : the number of character to append to seq1 to make seq2
    #    ie. A[0][i]
    v0 = [i for i in range(n+1)]

    v1 = [0 for _ in range(n+1)]
    for i in range(m):
        ##  v1 : current row distance from previous row v0

        # first element of v1 is A[i+1][0]
        # edit distance is delete (i+1) chars from seq1 to match empty seq2
        v1[0] = i + 1

        # formula to fill in the rest
        for j in range(n):
            # cost for A[i+1][j+1]
            deletion_cost = v0[j+1] + 1
            insertion_cost = v1[j] + 1

            if seq1[i] == seq2[j]:
                substitution_cost = v0[j]
            else:
                substitution_cost = v0[j] + 1

            v1[j+1] = min(min(deletion_cost, insertion_cost), substitution_cost)


        # copy v1 (current row) to v0 for next iteration
        v0 = [v1[i] for i in range(n+1)]

    # after the last swap, results are in v0
    return v0[n]

def test(s1, s2):
    print( f"ham({s1},{s2}) = {hamming(s1, s2)}" )
    print( f"lev({s1},{s2}) = {levenshtein(s1, s2)}" )


if __name__ == "__main__":
    s0 = ""
    s01 = "A"
    s1 = "Hello les gens ca va?"
    s11 = "He"
    s2 = "Hellolesgens ca va?"
    s3 = "Hello las gans va va?"
    s4 = "Hello les gens va ca?"

    all_strings = [s0, s01, s1, s11, s2, s3, s4]

    for s1 in all_strings:
        print( f"******* Dist from {s1}" )
        for s2 in all_strings:
            test(s1, s2)
