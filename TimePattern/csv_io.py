#!/usr/bin/env python3

"""
Lit et Ecrit, sous forme CSV, les données issues des annotation Psyphine
du type AnnotationsCrapel2023

Données .xlsx => .csv en faisant 'Sauver sous' dans LibreOffice Calc
"""

import csv

# ******************************************************************************
id_time =  0
id_actor = 1
id_tier = 2
id_annot = 3
id_value = 4
id_list_v = 5
id_raw_value = 6

list_anotators = ["AB", "AD", "FV", "GN", "JB", "MR", "VA", "YB"]
list_actors = ["N", "G"]
# removing 'default', 'lampe', 'preanalyse'
list_tiers = ['postures', 'gestes', 'regards', 'emotions', 'expressions']

vocab_empty = "-"

vocab_postures = [ ["avance la tête", "AT"],
                   ["lève la tête",   "LT"],
                   ["baisse la tête", "BT"],
                   ["penche la tête", "PT"],
                   ["hoche la tête de haut en bas", "HTH"],
                   ["hoche la tête de gauche à droite", "HTG"],
                   ["se penche", "P"],
                   ["se balance", "B"],
                   ["s'accroupit, se baisse", "AB"],
                   ["saute",          "S"],
                   ["danse",          "D"],
                   ["se retourne", "R"],
                   ["avance", "Av"],
                   ["avance très près", "AvP"],
                   ["revient en face de la lampe", "F"],
                   ["recule", "Re"],
                   ["recule très loin", "ReL"],
                   ["se place sur le côté", "PC"],
                   ["se place derrière", "PD"],
                   ["se tourne vers l'autre", "TA"],
                   ["se place derrière l'autre", "DA"],
                   ["se place devant autre", "AA"],
                   ["[autre]", "?"]]

vocab_gestes = [ ["mains dans le dos", "MD"],
                 ["mains le long du corps", "ML"],
                 ["mains dans les poches", "MP"],
                 ["mains sur les hanches", "MH"],
                 ["se tient les mains", "MT"],
                 ["main sur avant bras", "MB"],
                 ["bras croisés", "BC"],
                 ["paumes vers le ciel", "PC"],
                 ["hausse les épaules", "HE"],
                 ["se tient le menton", "TM"],
                 ["main devant la lampe", "MDL"],
                 ["agite main(s) devant la lampe", "AML"],
                 ["éloigne sa main de la lampe", "EML"],
                 ["tape des mains ou du pied", "TMP"],
                 ["tapote un support", "TS"],
                 ["touche l'autre", "TA"],
                 ["pousse l'autre", "PA"],
                 ["pointe la lampe", "PoL"],
                 ["pointe l'autre", "PoA"],
                 ["pointage (ni la lampe ni l'autre)", "Po"],
                 ["claque les doigts", "CD"],
                 ["utilise un artefact", "UA"],
                 ["amorce", "AM"],
                 ["[autre]", "?"],
                ]

vocab_regards = [ ["vers la lampe (indéterminé)", "L"],
                  ["vers le haut de la lampe",    "LH"],
                  ["vers le bas de la lampe",     "LB"],
                  ["vers la lampe en coin",       "LC"],
                  ["vers l'autre",                "A"],
                  ["vers l'autre en coin",        "AC"],
                  ["derrière soi",                "D"],
                  ["lève les yeux au ciel",       "YC"],
                  ["vers le sol",                 "S"],
                  ["vers la caméra",              "C"],
                  ["vers l'extérieur (de l'expérience)", "E"],
                  ["[autre]",                     "?"],
                 ]

vocab_emotions = [ ["amusement", "AM"],
                   ["colère", "CO"],
                   ["embarras", "EM"],
                   ["ennui", "EN"],
                   ["indifférence", "IN"],
                   ["perplexité", "PER"],
                   ["curiosité", "CU"],
                   ["peur", "PEU"],
                   ["dégout", "DE"],
                   ["surprise", "SU"],
                   ["tristesse", "TR"],
                   ["[autre]", "?"],
                  ]

vocab_expressions = [ ["fronce les sourcils", "FS"],
                      ["hausse les sourcils", "HLS"],
                      ["hausse un sourcil", "HUS"],
                      ["clin d'oeil", "CO"],
                      ["yeux ronds", "YR"],
                      ["yeux plissés", "YP"],
                      ["yeux fermés", "YF"],
                      ["bouche ouverte", "BO"],
                      ["sourire", "SO"],
                      ["grimace", "GR"],
                      ["lèvres vers le bas", "LB"],
                      ["[autre]", "?"],
                     ]

vocab_all = { 'postures': vocab_postures,
              'gestes': vocab_gestes,
              'regards': vocab_regards,
              'emotions': vocab_emotions,
              'expressions': vocab_expressions }

# ******************************************************************************
def read_raw_csv( filename ):
    """
    Lit le fichier sauvé par Excel/Calc et filtre les données
    - t entre 180 et 300

    Returns:
    [[time(s::int), acteur(string), tier(string), annotateur(string), annotation(string)]]
    """

    raw_data = []
    with open(filename) as fcsv:
        reader = csv.reader(fcsv)
        for row in reader:
            raw_data.append(row)

    data = []
    # real data begins in line 4 => at 0 avec mes données
    # TODO attention au BON début
    for raw_line in raw_data:
        # for annotation between 18000 ms and 30000 ms
        print( f"read={raw_line}" )
        t = int(raw_line[0]) // 1000
        print( f"  t={t}" )
        if t >= 180 and t < 300:
            data.append( [t,
                          raw_line[id_actor],
                          raw_line[id_tier],
                          raw_line[id_annot],
                          raw_line[id_raw_value]] )

    return data

# ******************************************************************************
def get_tier( data, tier_name, actor ):
    """
    Cherche les données pour un Tier et un Acteur donné.

    Returns:
    [[time(s::int), acteur(string), tier(string), annotateur(string), annotation(string)]]
    """

    filtered_data = [row for row in data
                     if row[id_tier] == tier_name and row[id_actor] == actor]
    return filtered_data

# ******************************************************************************
def list_unique_in_column( data, id_column ):
    """
    Liste tous les éléments différents de la column d'indice 'id_column'

    Returns:
    [unique_element]
    """

    items = []
    for row in data:
        if row[id_column] not in items:
            items.append(row[id_column])

    return items

# ******************************************************************************
def clean_vocab(data, vocabulary_d):
    """
    Pour gérer les éventuel doublons ou redite ou aggrégation dans les annotations
    ajoute une liste de "initiales_clefs", sans doublons

    Params:
    - tier_vocab : liste du vocabulaire avec initiales_clefs associés

    Returns:
    [[time(s::int), acteur(string), tier(string), annotateur(string), annotation(string),
     [InitialesVoab]]
    """
    for row in data:
        tier_name = row[id_tier]

        if tier_name in vocabulary_d:
            tier_vocab = vocabulary_d[tier_name]

            voc = []
            values = row[id_value]
            for v in tier_vocab:
                # print( f"testing {v[0]}" )
                if values.find(v[0]) >= 0:
                    voc.append(v[1])
            if voc == []:
                print( f"WARN: vocabulaire pas reconnu {values} => voc={tier_name}" )
                print( f"      {row}" )
            row.append(voc)
        # else:
        #     print( f"Pas de nettoyage pour {tier_name}" )

    return data

def seq_from_data( data, tier, actor, anotator ):
    """
    On filtre avec ces infos et construit la séquence ensuite

    Returns;
    [[MotClefVoca]]
    """

    tier_act_anot_data = [row for row in data
                          if row[id_tier] == tier and
                          row[id_actor] == actor and
                          row[id_annot] == anotator]

    return build_sequence(tier_act_anot_data)

def build_sequence( tier_act_anot_data ):
    """
    Construit la séquence avec toutes les secondes, en mettant [vocab_empty]
    comme listMotClef si pas d'annotation pour cette seconde précise

    Returns:
    [[MotClefsVocab]]
    """
    seq = []

    cur_time = 180
    for row in tier_act_anot_data:
        next_time = row[id_time]
        # Remplis avec [vocab_empty] jusqu'à l prochaine annotation
        if next_time != cur_time:
            for _ in range(cur_time, next_time):
                seq.append([vocab_empty])
        seq.append(row[id_list_v])
        cur_time = next_time+1

    # up to 300
    for _ in range(cur_time, 300):
        seq.append([vocab_empty])

    return seq

# ******************************************************************************
def pairdist( seq_1, seq_2):
    """
    Calcule la distance entre 2 séquences de [MotClefsVocab]

    Returns:
    - dist : int
    """

    if len(seq_1) != len(seq_2):
        print( "Pas possible de calculer les distances" )
        print(f"  len_1={len(seq_1)} et len_2={len(seq_2)}")
        return -1

    dist = 0
    for i in range(len(seq_1)):
        # Distance spéciale
        # d=1 si aucun des MotClef de anot_1 n'est dans MotClef de anot_2
        for kw in seq_1[i]:
            if kw in seq_2[i]:
                break
            dist += 1

    return dist

# ******************************************************************************
if __name__ == "__main__":
    # data = read_raw_csv("UneLigneParSeconde.csv")
    data = read_raw_csv("crapel_22_new.csv")
    print( data[0:5] )
    fd = get_tier(data, "regards", "N")
    print( fd[0:5] )
    # voc = list_vocabular(fd)
    # print( voc )
    # cl_tier = clean_tier_vocab(fd, vocab_regards)
    # print( cl_tier[0:20] )

    print( "******* TIERS" )
    print( list_unique_in_column(data, id_tier))

    ## Lister tout le vocabulaire
    # for tier_name in list_tiers:
    #     for actor_name in list_actors:
    #         t_a_data = get_tier(data, tier_name, actor_name)
    #         voc = list_unique_in_column(t_a_data, id_value)
    #         print( f"***** VOC for {tier_name}-{actor_name}" )
    #         print( voc )

    ## Cleanup data
    clean_data = clean_vocab(data, vocab_all)
    print( "***** CLEAN data" )
    for row in clean_data[:50]:
        print(row)

    ## pairwise distance for every tier x actor
    # also store all sequences
    all_sequences = []
    pdist = []
    for tiername in list_tiers:
        for actorname in list_actors:
            print( f"***** Pairwise for {tiername} x {actorname}" )
            list_seq = [seq_from_data(clean_data, tiername, actorname, a) for a in list_anotators]

            # add ("tier_act_anotator", seq) to all_sequences
            for anot, seq in zip(list_anotators, list_seq):
                all_sequences.append( ( tiername+"_"+actorname+"_"+anot, seq ) )

            for i in range(len(list_anotators)):
                for j in range(i+1, len(list_anotators)):
                    pd = pairdist( list_seq[i], list_seq[j] )
                    print( f"d({list_anotators[i]}, {list_anotators[j]})={pd}" )
                    pdist.append( [tiername, actorname, list_anotators[i], list_anotators[j],
                                   pd])

    print( "***** Pariwise distance" )
    for row in pdist[:20]:
        print( row )


    with open('pairwise.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(pdist)

    # build the CSV iterable for sequences (1 column for each sequence)
    data_seq = []
    seq_names = [item[0] for item in all_sequences]
    data_seq.append( seq_names )

    len_sequence = len(all_sequences[0][1])
    for i in range(len_sequence):
        row = []
        for name_seq in all_sequences:
            row.append( name_seq[1][i] )
        data_seq.append( row  )

    with open('sequences.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(data_seq)

    # # sequence de JB et AD
    # seq_JB = build_sequence( [row for row in cl_tier if row[id_annot] == "JB"] )
    # data_AD = [row for row in cl_tier if row[id_annot] == "AD"]
    # print( "***** AD regards" )
    # for i in range(len(data_AD)):
    #     row = data_AD[i]
    #     print(f"{i}: {row[id_time]} = {row[id_list_v]}")
    # seq_AD = build_sequence(data_AD)
    # print( f"***** AD regards as a SEQUENCE ({len(seq_AD)})" )
    # for i in range(len(seq_AD)):
    #     print(f"{i}: {seq_AD[i]} -- {seq_JB[i]}")

    # d = pairdist( seq_AD, seq_JB)
    # print( f"d(AD, JB)={d}" )

    # list_seq = [seq_from_data(data, "regards", "N", a) for a in list_anotators]
    # # for i in range(120):
    # #     row_str = f"{i:3d} : "
    # #     for s in list_seq:
    # #         row_str += f"{str(s[i]):15s}"
    # #     print( row_str )

    # pdist = []
    # for i in range(len(list_anotators)):
    #     for j in range(i+1, len(list_anotators)):
    #         pdist.append( [list_anotators[i], list_anotators[j],
    #                        pairdist( list_seq[i], list_seq[j] )])
    # print( "***** Pariwise distance" )
    # for row in pdist:
    #     print( row )

    # TODO clean vocabulary on basic data, before filtering ?
