# Les differents graphes

## fig_seq_*

pour chaque couple (tier, acteur), par exemple, postures_N, on affiche le choix des différents annotateurices => les *annotations* sont en ordonnées.
Une couleur par *annotateur*
Le code des annotations est donné ci-dessous

## fig_anot_*

pour chaque couple (tier, acteur), par exemple, postures_N, on affiche le choix des différents annotateurices => les *annotateurices* sont en ordonnées.
Une couleur par *annotation*
Le code des annotations est donné ci-dessous

## fig_pairwise.pdf

La distance entre deux annotations est le nombre de fois (en 2 minuutes, soit en 120 secondes) que les deux annotateurices n'ont pas fait la même annotation, pour un couple (tier, actor) donné.

Imaginons que, pour les premières secondes de (regards_N), on ait les anotations suivantes:
AD : "['YR']"  "['HL']"  "['HL']" "['-']"   "['HLS']" "['HLS']"
FV : "['YR']"  "['YR']"  "['HLS']" "['-']"   "['HLS']" "['HLS']"

la distance entre les deux serait de 2 (les anotations 2 et 3 sont différentes, les autres sont les mêmes)

Et donc la figure, pour chaque couple (tier, actor) en abscisse, fait un "box plot" des différentes distances entre toutes les paires d'annotateurises. Le rectangle principal contient la majorité des distances, le trait horizontal est la moyenne et les extrémités indiquent l'intervale de variation de distance. Les éventuels points/ronds en dehors des intervalles indiques des distances qui paraissent "abérantes"

## Les code des annotations

> vocab_postures = [ ["avance la tête", "AT"],
>                    ["lève la tête",   "LT"],
>                    ["baisse la tête", "BT"],
>                    ["penche la tête", "PT"],
>                    ["hoche la tête de haut en bas", "HTH"],
>                    ["hoche la tête de gauche à droite", "HTG"],
>                    ["se penche", "P"],
>                    ["se balance", "B"],
>                    ["s'accroupit, se baisse", "AB"],
>                    ["saute",          "S"],
>                    ["danse",          "D"],
>                    ["se retourne", "R"],
>                    ["avance", "Av"],
>                    ["avance très près", "AvP"],
>                    ["revient en face de la lampe", "F"],
>                    ["recule", "Re"],
>                    ["recule très loin", "ReL"],
>                    ["se place sur le côté", "PC"],
>                    ["se place derrière", "PD"],
>                    ["se tourne vers l'autre", "TA"],
>                    ["se place derrière l'autre", "DA"],
>                    ["se place devant autre", "AA"],
>                    ["[autre]", "?"]]
>
> vocab_gestes = [ ["mains dans le dos", "MD"],
>                  ["mains le long du corps", "ML"],
>                  ["mains dans les poches", "MP"],
>                  ["mains sur les hanches", "MH"],
>                  ["se tient les mains", "MT"],
>                  ["main sur avant bras", "MB"],
>                  ["bras croisés", "BC"],
>                  ["paumes vers le ciel", "PC"],
>                  ["hausse les épaules", "HE"],
>                  ["se tient le menton", "TM"],
>                  ["main devant la lampe", "MDL"],
>                  ["agite main(s) devant la lampe", "AML"],
>                  ["éloigne sa main de la lampe", "EML"],
>                  ["tape des mains ou du pied", "TMP"],
>                  ["tapote un support", "TS"],
>                  ["touche l'autre", "TA"],
>                  ["pousse l'autre", "PA"],
>                  ["pointe la lampe", "PoL"],
>                  ["pointe l'autre", "PoA"],
>                  ["pointage (ni la lampe ni l'autre)", "Po"],
>                  ["claque les doigts", "CD"],
>                  ["utilise un artefact", "UA"],
>                  ["amorce", "AM"],
>                  ["[autre]", "?"],
>                 ]
>
> vocab_regards = [ ["vers la lampe (indéterminé)", "L"],
>                   ["vers le haut de la lampe",    "LH"],
>                   ["vers le bas de la lampe",     "LB"],
>                   ["vers la lampe en coin",       "LC"],
>                   ["vers l'autre",                "A"],
>                   ["vers l'autre en coin",        "AC"],
>                   ["derrière soi",                "D"],
>                   ["lève les yeux au ciel",       "YC"],
>                   ["vers le sol",                 "S"],
>                   ["vers la caméra",              "C"],
>                   ["vers l'extérieur (de l'expérience)", "E"],
>                   ["[autre]",                     "?"],
>                  ]
>
> vocab_emotions = [ ["amusement", "AM"],
>                    ["colère", "CO"],
>                    ["embarras", "EM"],
>                    ["ennui", "EN"],
>                    ["indifférence", "IN"],
>                    ["perplexité", "PER"],
>                    ["curiosité", "CU"],
>                    ["peur", "PEU"],
>                    ["dégout", "DE"],
>                    ["surprise", "SU"],
>                    ["tristesse", "TR"],
>                    ["[autre]", "?"],
>                   ]
>
> vocab_expressions = [ ["fronce les sourcils", "FS"],
>                       ["hausse les sourcils", "HLS"],
>                       ["hausse un sourcil", "HUS"],
>                       ["clin d'oeil", "CO"],
>                       ["yeux ronds", "YR"],
>                       ["yeux plissés", "YP"],
>                       ["yeux fermés", "YF"],
>                       ["bouche ouverte", "BO"],
>                       ["sourire", "SO"],
>                       ["grimace", "GR"],
>                       ["lèvres vers le bas", "LB"],
>                       ["[autre]", "?"],
>                      ]

## D'un point de vue technique

- générer les fichier crapel_22_new.csv à partir de .eaf : python eaf_io.py

- génerer les autres fichiers .csv : python3 csv_io.py

Ensuite, je visualise avec R : https://www.r-project.org/

- library(ggplot2)
- source( "plot_pairwise.R" )
- dp <- load_pairwise( "pairwise.csv" )
- pp <- plot_pairwise_box(dp)
- ggsave( "fig_pairwise.pdf", plot=pp, units="mm", width=290)
-
- ds <- load_sequence( "sequences.csv" )
- make_all_plot_sequences( ds, unique(dp$tier), unique(dp$actor), anot_names )
