#!/usr/bin/env python3

"""
Lit données depuis fichier .eaf et prépare .csv avec données par 'delta_t'
"""

import re

import pympi.Elan as Elan

# ******************************************************************************
def split_tiername( tiername ):
    """
    Split "CRA_22.exp03_N-expr.faciales-AD" en ["N", "expressions", "AD"]
    """

    # remove up to 'exp03_' or 'exp03'
    id = tiername.find( "exp03_" )
    tmp_str = ""
    if id < 0:
        id = tiername.find( "exp03" )
        if id < 0:
            return None
        else:
            tmp_str = tiername[id+5:]
    else:
        tmp_str = tiername[id+6:]
    # print( f"Working with {tmp_str}")


    tokens = re.split( r'-|_', tmp_str )
    # print( f"tok={tokens}" )

    if tokens[1] == "expr.faciales":
        tokens[1] = "expressions"

    return tokens

# ******************************************************************************
def append_tier( eaf, tiername, data ):
    """
    Ajoute toutes les anotation du tier sous la forme
    [tier, actor, anotator, tstart, tend, anotations]
    """
    tok = split_tiername( tiername )
    if tok is None:
        print( f"Peux pas spliter {tiername}" )
        return

    ts = eaf.timeslots

    # v is (tstart, tend, anotations)
    for k, v in eaf.tiers[tiername][0].items():
        data.append( [tok[0], tok[1], tok[2], ts[v[0]], ts[v[1]], v[2]] )

# ******************************************************************************
def append_at_time( data_anotation, time_instant, data_per_second):
    """
    Parcourt toutes les annotation et ajoute si time_instant est dans l'intervalle
    """

    for row in data:
        if row[3] <= time_instant and row[4] > time_instant:
            data_per_second.append( [time_instant]+row )


# ******************************************************************************
# ******************************************************************************
eaf_obj = Elan.Eaf(file_path="crapel_2022_VC_last.eaf")

prop = eaf_obj.get_properties()
for k, v in prop:
    print( f"{k} => {v}" )

voc = eaf_obj.get_controlled_vocabulary_names()
print( f"voc = {voc}")
print( f"cvoc entries = {eaf_obj.get_cv_entries('VC-gestes')}" )

ref = eaf_obj.get_external_ref_names()
print( f"ref = {ref}" )

t = eaf_obj.get_full_time_interval()
print( f"full_time = {t}" )

lex = eaf_obj.get_lexicon_ref_names()
print( f"lex = {lex}" )

link = eaf_obj.get_linked_files()
print( f"link = {link}" )

tier = eaf_obj.get_tier_names()
print( f"tier = {tier}" )
for t in list(tier):
    print( "  ", t, split_tiername(t) )

data = []
for tiername in list(tier):
    append_tier(eaf_obj, tiername, data)

for row in data[0:5]:
    print(row)

# compute min max timeslot
start_time = 300000
end_time = 0
for _, timeslot in eaf_obj.timeslots.items():
    if timeslot < start_time:
        start_time = timeslot
    if timeslot > end_time:
        end_time = timeslot
print( f"start_time={start_time} end_time={end_time}" )

print( "***** Data per seconds" )
data_seconds = []
for t in range(start_time, end_time, 1000):
    append_at_time( data, t, data_seconds )
for i in range(len(data_seconds)):
    print( f"{i:3d} : {data_seconds[i]}")

import csv
with open('crapel_22_new.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerows(data_seconds)

# # an = eaf_obj.get_ref_annotation_data_between_times('CRA_22.exp03_N-expr.faciales-AD',
# #                                                    180000, 190000)
# an = eaf_obj.get_ref_annotation_data_for_tier( 'default' )
# # an = eaf_obj.get_ref_annotatio'CRA_22.exp03_N-expr.faciales-AD')n_data_for_tier(0)
# print( an )
# print( f"an = {an}" )

# td = eaf_obj.get_parameters_for_tier('default')
# print(td)

# an_ref = eaf_obj.get_ref_annotation_data_after_time( 'default', 0 )
# print(an_ref)

# print( eaf_obj.tiers['CRA_22.exp03_N-expr.faciales-AD'] )
# print( "0\n", eaf_obj.tiers['CRA_22.exp03_N-expr.faciales-AD'][0] )
# ts = eaf_obj.timeslots
# for k, v in eaf_obj.tiers['CRA_22.exp03_N-expr.faciales-AD'][0].items():
#     print( k, v )
#     print( f"{ts[v[0]]} -> {ts[v[1]]} = {v[2]}" )
# print( "0\n", eaf_obj.tiers['CRA_22.exp03_N-expr.faciales-AD'][0]['a1'] )
# print( "1\n", eaf_obj.tiers['CRA_22.exp03_N-expr.faciales-AD'][1] )
# print( "2\n", eaf_obj.tiers['CRA_22.exp03_N-expr.faciales-AD'][2] )
