#!/usr/bin/env python3


# External modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import PolyCollection

# Globales
n = 20  # size of sequence
L = 5   # number of filters
lambda_IIR = 0.7

# impulse
i = [0 for _ in range(n)]
i[0] = 1

# step
s = [1 for _ in range(n)]


# ******************************************************************************
def identity( x ):
    y = [0 for _ in range(n)]
    for k in range(1, n):
        y[k] = x[k]

    return x

# ******************************************************************************
def iir_filter( x, lbda=0.5):
    y = [0 for _ in range(n)]
    for k in range(1, n):
        y[k] = (1.0 - lbda) * y[k-1] + lbda * x[k-1]

    return y

# ******************************************************************************
def laguerre0_filter( x, lbda=0.5 ):
    y = [0 for _ in range(n)]
    coef_x = np.sqrt(1.0 - (1.0 - lbda)*(1.0 - lbda))

    for k in range(1, n):
        y[k] = (1.0 - lbda) * y[k-1] + coef_x * x[k-1]

    return y

# ******************************************************************************
def laguerreN_filter( x, lbda=0.5 ):
    y = [0 for _ in range(n)]

    for k in range(1, n):
        y[k] = (1.0 - lbda) * y[k-1] + x[k-1] - (1.0 - lbda) * x[k]

    return y

# ******************************************************************************
if __name__ == "__main__":
    print(f"imp={i}")

    y = [i]
    y.append(identity(y[-1]))

    for id_od in range(L+1):
        y.append( iir_filter(y[-1], lbda=lambda_IIR))
        print(f"IIR(order={id_od})={y[-1]}")


    # plotting
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    xp = np.arange(0, n)
    yp = np.ones((n, 1))
    for id_od in range(0, L+1):
        line, = ax.plot( xp, yp*id_od, y[id_od] )

        vertices = [list(zip(np.pad(xp, (1, 1), mode='edge'),
                             np.pad(y[id_od], (1, 1))))]
        print( f"vert={vertices}" )
        poly = PolyCollection( vertices, facecolor='0.9', edgecolor=line.get_color() )
        poly.set_alpha(1)
        ax.add_collection3d( poly, zs=id_od, zdir='y' )

    plt.show()

    y = [i]
    y.append(laguerre0_filter( y[-1], lbda=lambda_IIR))

    for id_od in range(L+1):
        y.append( laguerreN_filter(y[-1], lbda=lambda_IIR))
        print(f"IIR(order={id_od})={y[-1]}")


    # plotting
    fig = plt.figure()
    ax = fig.add_subplot(111)

    xp = np.arange(0, n)
    yp = np.ones((n, 1))
    for id_od in range(0, L+1):
        line, = ax.plot( xp, y[id_od] )

    plt.show()

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    xp = np.arange(0, n)
    yp = np.ones((n, 1))
    for id_od in range(0, L+1):
        line, = ax.plot( xp, yp*id_od, y[id_od] )

        vertices = [list(zip(np.pad(xp, (1, 1), mode='edge'),
                             np.pad(y[id_od], (1, 1))))]
        print( f"vert={vertices}" )
        poly = PolyCollection( vertices, facecolor='0.9', edgecolor=line.get_color() )
        poly.set_alpha(1)
        ax.add_collection3d( poly, zs=id_od, zdir='y' )

    plt.show()
