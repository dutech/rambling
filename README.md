# Various things in various programming languages

## Cpp
- [ ] use waf
- [ ] add Readme
- [ ] add from PTB

## R
- [ ] add Readme

## Haskel
- [ ] voir/comprendre ListPicker et Tokens, dans Main
- [ ] voir/comprendre test-apply
- [ ] voir/comprendre test-generate
- [ ] voir/comprendre test-monad
- [ ] voir/comprendre test-state
- [ ] voir/comprendre test-token
- XP_Monad : example of using MonadPlus to list all success/fail paths
