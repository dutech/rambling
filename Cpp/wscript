#!/usr/bin/env python
# encoding: utf-8
# waf configuration file .waf --help pour commande par défaut
# Utilisé par CMD dist

## Configured to use:
## - try_cmath
## - stop_screensaver

## Par défaut ./waf configure va utiliser buildir=wbuild et CXX=g++
## MAIS avec ./waf configure --out=cbuild --check-cxx-compiler=clang++
##      on peut utilise clang :o)
## l'option --color permet de coloriser la sortie du compilo

from waflib.Errors import ConfigurationError

APPNAME = 'CppRambling'
VERSION = '0.1'

# root, build dir
top = '.'
#out = 'wbuild'
out = 'cbuild'

opt_flags = '-O3'
debug_flags = '-O0 -g'

# ************************************************************************* help
def help(ctx):
    print( "**** WAF for CppRambling, usual commands ************" )
    print( "configuration :  ./waf configure --out=wbuild [--use_clang => --check_cxx_compiler=clang++]" )
    print( "                       --prefix=[/usr/local]" )
    print( "default       :  ./waf configure --out=cbuild --check-cxx-compiler=clang++ --compil_db" )
    print( "build :          ./waf build ")
    print( "build specific : ./waf build --targets=test/001-curve" )
    print( "compdb :         ./waf compdb" )
    print( "clean :          ./waf clean" )
    print( "detailed help :  ./waf --help or see https://waf.io/book" )
    print( "  options :      --compil_db --debug" )

# ********************************************************************** options
def options( opt ):
    opt.load( 'compiler_cxx' )

    # option use_clang
    opt.add_option('--use_clang', dest='use_clang', action="store_true", default=True,
                   help='use clang and compile in cbuild (replace --out --check_cxx_compiler)' )

    # option debug
    opt.add_option('--debug', dest='debug', action="store_true", default=False,
                   help='compile with debugging symbols' )

    # clang compilation database
    opt.add_option('--compil_db', dest='compil_db', action="store_true", default=True,
                   help='use clang compilation database' )

    # define some macro for C++
    # (equivalent to #define LABEL or -DLABEL to compilator
    opt.add_option('-D', '--define', action="append", dest="defined_macro",
                   help='define preprocessing macro' )

# **************************************************************** CMD configure
def configure( conf ):
    print( "__START CONFIGURATION *******************************************" )
    print( "by defaul, option --compil_db is ON" )
    # print( conf.env)
    # print( "******************************************************************")

    #memorise appname as it is not easily recoverable later
    conf.env.appname = APPNAME
    conf.env.version = VERSION

    if conf.options.use_clang:
        conf.options.check_cxx_compiler = "clang++"
        # 'out' cannot be without option --out as it is checked BEFORE reading wscript

    # Ensure that clang++ is used for compil_db, must be done before loading _cxx
    # if conf.options.compil_db:
    #     conf.options.check_cxx_compiler = 'clang++'

    ## set env and options according to the possible compilers
    ## better to set options 'out' and 'check_cxx_compiler' before loading
    conf.load( 'compiler_cxx' )
    # print( "__CXX__" )
    # print( conf.env)
    # print( "******************************************************************")

    if conf.options.compil_db:
        ## To generate 'compile_commands.json' at the root of buildpath
        # to be linked/copied in order to user 'cquery' in emacs through lsp-mode
        # see https://github.com/cquery-project/cquery/wiki/Emacs
        conf.load('clang_compilation_database', tooldir="ressources")
        print( "CXX=",conf.env.CXX)
        # try:
        #     conf.find_program('compdb', var='COMPDB')
        # except ConfigurationError as e:
        #     raise ConfigurationError( msg=e.msg+"\ncompdb not found, install using 'pip install compdb'" )

    # conf.env['CXXFLAGS'] = ['-D_REENTRANT','-Wall','-fPIC','-std=c++11']
    # conf.env['CXXFLAGS'] = ['-Wall','-std=c++17']
    conf.env['CXXFLAGS'] = ['-Wall','-std=c++20']
    ##DELconf.env.INCLUDES_JSON = conf.path.abspath()+'/include'

    ## Require X11, using wraper around pkg-config
    conf.check_cfg(package='x11',
                   uselib_store='X11',
                   args=['--cflags', '--libs']
    )
    ## Require X11 extensions using wraper around pkg-config
    conf.check_cfg(package='xext',
                   uselib_store='XEXT',
                   args=['--cflags', '--libs']
    )

    ## Require DBus, using wraper around pkg-config
    conf.check_cfg(package='dbus-1',
                   uselib_store='DBUS',
                   args=['--cflags', '--libs']
    )
    # print( "__AFTER__" )
    # print( conf.env )
    # print( "******************************************************************")

    print( "__END of CONFIGURATION *********************************" )
    print( f"out = {out} env={conf.options.out}" )
    # print( f"compdb={conf.env.COMPDB}" )

def compdb(ctx):
    """ Should be called after build, one compile_commands.json exists in 'out' dir"""
    # ENV is not accessible in a custom command
    print( "Runnin COMPDB after build" )
    if ctx.options.compil_db:
        # ctx.exec_command( f"{ctx.env.COMPDB[0]} -p {out} list > compile_commands.json" )
        ctx.exec_command( f"compdb -p {out} list > compile_commands.json" )
    
# ******************************************************************** CMD build
def build( bld ):
    print('→ build from ' + bld.path.abspath())

    # check debug option
    if bld.options.debug:
        bld.env['CXXFLAGS'] += debug_flags.split(' ')
    else:
        bld.env['CXXFLAGS'] += opt_flags.split(' ')
    print( bld.env['CXXFLAGS'] )
        
    # add macro defines as -D options
    # print( "MACRO=", bld.options.defined_macro )
    if bld.options.defined_macro:
        # print( "ENV=", bld.env.DEFINES )
        bld.env.DEFINES = bld.env.DEFINES + bld.options.defined_macro
        # print( "new ENV=", bld.env.DEFINES )

    # if comp_db flag, run compdb after successful build
    # BUT population of out/compile_commands.json is in fact not done yet...
    # bld.add_post_fun( run_compdb )


    # TODO install a post_build function that generate a proper compile_commands.json
    # with *also* the header files from the compile_commands.json generated by clang
    # but only for the .cpp files
    # use "compdb" for that
    # see https://github.com/Sarcasm/compdb#generate-a-compilation-database-with-header-files
    # bld.recurse( 'test' )

    def build_file( file, to_include=[], to_addlib=[], to_use=[] ):
        print( "** Buildind "+file )
        bld.program(
            source =   [file],
            target =   file[:-4],
            includes = [".",] + to_include,
            lib =     to_addlib, # ['X11','Xss'],#["m","rt","dl"], # X11 screensaver
            use =     to_use,
        )


    build_file( "try_cmath.cpp" )
    build_file( "stop_screensaver.cpp", to_addlib=['X11', 'Xss'] )
    build_file( "dbus.cpp", to_use=['DBUS'] )
    build_file( "dbus_inhibit.cpp", to_use=['DBUS'] )
    build_file( "x11_dpms.cpp", to_use=['X11', 'XEXT'] )
    build_file( "x11_screensaver.cpp", to_use=['X11'] )
