/**
 * Use X11 to gather info and disable re-enable X11 core screensaver.
 */

#include <iostream>
#include <X11/Xlib.h>
#include <cstring>

int main(int argc, char *argv[])
{
    bool _arg_onlyinfo {true};
    bool _arg_disable {false};
    // stop if the 1 argument is not "enable" or "disable"
    if (argc > 1) {
        if (std::strcmp("info", argv[1]) == 0) {
            _arg_onlyinfo = true;
        }
        else if (std::strcmp("enable", argv[1]) == 0) {
            _arg_onlyinfo = false;
            _arg_disable = false;
        }
        else if (std::strcmp("disable", argv[1]) ==0) {
            _arg_onlyinfo = false;
            _arg_disable = true;
        }
        else {
            std::cerr << "Error : arg " << argv[1] << " unknown" << std::endl;
            std::cerr << "usage : " << argv[0] << " [info/enable/disable]" << std::endl;
            exit(1);
        }
    }


    // Connection to Xerver to get the display
    Display *display = XOpenDisplay( NULL ); // NULL : get $DISPLAY env varialble
    char* display_string = DisplayString(display);
    std::cout << "__Connection to XServer for display=" << display_string << std::endl;
    int nb_screen = XScreenCount(display);
    std::cout << "  has " << nb_screen << " screens" << std::endl;

    for (int ids = 0; ids < nb_screen; ++ids) {
        std::cout << "  __Screen " << ids;
        std::cout << " height=" << XDisplayHeight(display, ids)
                  << "(" << XDisplayHeightMM(display, ids) << " mm)";
        std::cout << " width=" << XDisplayWidth(display, ids)
                  << "(" << XDisplayWidthMM(display, ids) << " mm)";
        std::cout << std::endl;
    }

    int timeout_return, interval_return, prefer_blanking, allow_exposure;
    XGetScreenSaver(display, &timeout_return, &interval_return,
                    &prefer_blanking, &allow_exposure);
    std::cout << "__X11 core screensaver" << std::endl;
    std::cout << "  timeout before going up = " << timeout_return << "s" << std::endl;
    std::cout << "  interval at which random pattern are changed = " << interval_return << "s" << std::endl;
    std::cout << "  mode = ";
    switch (prefer_blanking) {
        case DontPreferBlanking:
            std::cout << "DontPreferBlanking" << std::endl;
            break;
        case PreferBlanking:
            std::cout << "PreferBlanking" << std::endl;
            break;
        case DefaultBlanking:
            std::cout << "DefaultBlanking" << std::endl;
            break;
    }
    std::cout << "  exposures = ";
    switch (allow_exposure) {
        case DontAllowExposures:
            std::cout << "DontAllowExposures" << std::endl;
            break;
        case AllowExposures:
            std::cout << "AllowExposures" << std::endl;
            break;
        case DefaultExposures:
            std::cout << "DefaultExposures" << std::endl;
            break;
    }


    if (not _arg_onlyinfo) {
        if (_arg_disable) {
            XSetScreenSaver(display, 0 /* => disable*/, interval_return,
                            prefer_blanking, allow_exposure);
            std::cout << "__X11 core screensaver disabled" << std::endl;
        }
        else {
            XSetScreenSaver(display, -1 /*=> restore default*/, interval_return,
                            prefer_blanking, allow_exposure);
            std::cout << "__X11 core screensaver enabled" << std::endl;
        }
    }

    XCloseDisplay(display);

    return 0;
}
