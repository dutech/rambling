/**
 * Use X11 DisplayPowerManagementSystem (DMPS) to prevent blanck screen
 */

#include <iostream>
#include <X11/Xlib.h>
#include <X11/extensions/dpms.h>
#include <cstring>

int main(int argc, char *argv[])
{
    bool _arg_onlyinfo {true};
    bool _arg_disable {false};
    // stop if the 1 argument is not "enable" or "disable"
    if (argc > 1) {
        if (std::strcmp("info", argv[1]) == 0) {
            _arg_onlyinfo = true;
        }
        else if (std::strcmp("enable", argv[1]) == 0) {
            _arg_onlyinfo = false;
            _arg_disable = false;
        }
        else if (std::strcmp("disable", argv[1]) ==0) {
            _arg_onlyinfo = false;
            _arg_disable = true;
        }
        else {
            std::cerr << "Error : arg " << argv[1] << " unknown" << std::endl;
            std::cerr << "usage : " << argv[0] << " [info/enable/disable]" << std::endl;
            exit(1);
        }
    }


    // Connection to Xerver to get the display
    Display *display = XOpenDisplay( NULL ); // NULL : get $DISPLAY env varialble
    char* display_string = DisplayString(display);
    std::cout << "__Connection to XServer for display=" << display_string << std::endl;
    char* vendor = XServerVendor(display);
    std::cout << "  Xserver vendor=" << vendor << std::endl;
    int nb_screen = XScreenCount(display);
    std::cout << "  has " << nb_screen << " screens" << std::endl;

    for (int ids = 0; ids < nb_screen; ++ids) {
        std::cout << "  __Screen " << ids;
        std::cout << " height=" << XDisplayHeight(display, ids)
                  << "(" << XDisplayHeightMM(display, ids) << " mm)";
        std::cout << " width=" << XDisplayWidth(display, ids)
                  << "(" << XDisplayWidthMM(display, ids) << " mm)";
        std::cout << std::endl;
    }

    CARD16 power_level;
    BOOL dpms_state;

    if (!DPMSInfo(display, &power_level, &dpms_state)) {
        std::cerr << "__DPMS does not give info" << std::endl;
    }
    else {
        std::cout << "  DPMS state=";
        if (dpms_state == true) std::cout << "Enabled" << std::endl;
        else std::cout << "Disabled" << std::endl;

        std::cout << "  DPMS power=" << power_level << std::endl;
    }

    CARD16 standby_timeout, suspend_timeout, off_timeout;
    if (!DPMSGetTimeouts(display, &standby_timeout, &suspend_timeout, &off_timeout)) {
        std::cerr << "__DMPS could not get timeouts" << std::endl;
    }
    else {
        std::cout << "__DMPS" << std::endl;
        std::cout << "  standby_timeout=" << standby_timeout << std::endl;
        std::cout << "  suspend_timeout=" << suspend_timeout << std::endl;
        std::cout << "  off_timeout    =" << off_timeout << std::endl;
    }

    if (not _arg_onlyinfo) {
        if (_arg_disable) {
            DPMSDisable(display);
            std::cout << "__DPMS disabled" << std::endl;
        }
        else {
            DPMSEnable(display);
            std::cout << "__DPMS enabled" << std::endl;
        }
    }

    XCloseDisplay(display);

    return 0;
}
