#include <iomanip>
#include <iostream>

int main(int argc, char *argv[]) {

  std::cout << "__Boolean as true/false" << std::endl;
  std::cout << "  " << std::boolalpha << true << " is " << std::noboolalpha << true << std::endl;
  std::cout << "  " << std::boolalpha << false << " is " << std::noboolalpha << false << std::endl;

  return 0;
}
