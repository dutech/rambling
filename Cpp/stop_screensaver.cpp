/**
 * In the goal of preventing the screen saver from kicking in,
 * test some function to tell it to wait a bit longuer.
 *
 * From https://www.linuxquestions.org/questions/slackware-14/how-do-i-stop-the-screen-from-blanking-when-i-watch-1-hour-youtube-movies-4175478789/page2.html
 * I wanted to make sure I had my technical understanding right. Specifically:
 *
 * - the screen saver is supposed to call XScreenSaverQueryInfo to get
 *   the elapsed idle time and to decide when to activate
 * - the game or media player is supposed to call XResetScreenSaver to
 *   reset the elapsed idle time, and to delay the screen saver from activating
 */

// compile with : gcc sstest.c -lX11 -lXss

#include <X11/extensions/scrnsaver.h>
#include <unistd.h>
#include <stdio.h>

int main()
{
	int i;
	XScreenSaverInfo *ssi;
	Display *dpy;
	int event_basep;
	int error_basep;

	dpy = XOpenDisplay(0);
	XScreenSaverQueryExtension(dpy, &event_basep, &error_basep);

	ssi = XScreenSaverAllocInfo();

	for (i = 0; i < 10; i++)
	{
		sleep(1);
		XScreenSaverQueryInfo(dpy, DefaultRootWindow(dpy), ssi);
		printf("%d\n", ssi->idle / 1000);
		if (i == 5)
		{
			XResetScreenSaver(dpy);
		}

	}
	return 0;
}
