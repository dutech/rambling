#include <cmath>
#include <iostream>

// Partition in bins, upper bound in last bin
std::size_t bin(double x, double x_min=0.0, double x_max=1.0,
                std::size_t nb_bins=10 )
{
  if (x == x_max) return nb_bins-1;
  double reloc = (x - x_min) / (x_max - x_min) * static_cast<double>(nb_bins);
  return static_cast<std::size_t>(reloc);
}
// partition of an intervalle in X bins
void t_bin()
{
  std::cout << "__bin 0,1 in 15 ******************" << std::endl;
  for (double x=0.0; x<=1.0; x += 0.05) {
    std::cout << x << " => bin No " << bin(x, 0.0, 1.0, 15)<< std::endl;
  }
  std::cout << "1.0" << " => bin No " << bin(1.0, 0.0, 1.0, 15)<< std::endl;
}

// test fmod
void t_fmod()
{
  std::cout << "__fmod **************************" << std::endl;
  for (double x=-2.0; x<=3; x+=0.3) {
    std::cout << x << " fmod(1.5) = " << fmod(x,1.5)<< std::endl;
  }
}

int main(int argc, char *argv[]) {
  t_bin();
  t_fmod();

  return 0;
}
