/**
 * Taken from https://github.com/makercrew/dbus-sample
 * and https://dbus.freedesktop.org/doc/dbus-tutorial.html#members
 *
 * Steps to connect do DBus and
 * - ask for list of Inibitor process (for sleep)
 *
 * Reminders *******************************************************************
 * => list services on dbus
 * busctl list [--user]
 * => get methods for .login1
 * busctl introspect org.freedesktop.login1 /org/freedesktop/login1
 * => get methods for the .Manager interface
 * busctl introspect org.freedesktop.login1 /org/freedesktop/login1 org.freedesktop.login1.Manager
 * NAME                                TYPE      SIGNATURE          RESULT/VALUE
 * .ListInhibitors                     method    -                  a(ssssuu)
 * .Inhibit                            method    ssss               h                    -
 *
 * using gdbus
 * gdbus introspect --system --dest org.freedesktop.login1 --object-path /org/freedesktop/login1
 * ...
 *  ListInhibitors(out a(ssssuu) inhibitors);
 * ...
 * Inhibit(in  s what,
              in  s who,
              in  s why,
              in  s mode,
              out h pipe_fd);
 * ...
 *
 * See also
 * https://www.freedesktop.org/wiki/Software/systemd/inhibit/
 * https://www.freedesktop.org/wiki/Software/systemd/logind/
 */
#include <dbus/dbus.h>

#include <cstddef>  // NULL, offsetof, size_t, ptrdiff_t, nullptr_t, max_align_t, byte
#include <cstdio>   // perror, ...
#include <iostream> // cout
#include <unistd.h> // sleep, close
#include <cstring>   // strcmp

// ***************************************************************************
// ******************************************************************** Global
// ***************************************************************************
DBusError dbus_error;
DBusConnection * dbus_conn = nullptr;
DBusMessage * dbus_msg = nullptr;
DBusMessage * dbus_reply = nullptr;
const char * dbus_result = nullptr;
const char * dbus_result_signature = nullptr;

// ***************************************************************************
// *************************************************************** ListInhibit
// ***************************************************************************
void parse_dbus_inhibitstruct( DBusMessageIter *parent_iter,
                               const std::string& indent = "") {
    DBusMessageIter dbus_msg_subiter;
    ::dbus_message_iter_recurse(parent_iter, &dbus_msg_subiter);

    std::string header[] {"what", "who", "why", "mode", "UID", "PID"};

    std::cout << indent;

    int subcurrent_type;
    int arg_subcount {0};
    while ((subcurrent_type = dbus_message_iter_get_arg_type (&dbus_msg_subiter)) != DBUS_TYPE_INVALID) {
        //std::cout << "    subarg[" << arg_subcount << "] is " << subcurrent_type << std::endl;

        if (subcurrent_type == DBUS_TYPE_STRING ) {
            char* str = NULL;
            dbus_message_iter_get_basic(&dbus_msg_subiter, &str);
            std::cout << header[arg_subcount] << ": " << str << " / ";
        }
        else if (subcurrent_type == DBUS_TYPE_UINT32 ) {
            dbus_uint32_t number {0};
            dbus_message_iter_get_basic(&dbus_msg_subiter, &number);
            std::cout << header[arg_subcount] << ": " << number << " / ";
        }
        else {
            std::cout << header[arg_subcount] << ": " << "???" << " / ";
        }

        arg_subcount ++;
        dbus_message_iter_next (&dbus_msg_subiter);
    }
    std::cout << std::endl;
}
void parse_reply_listinhib( DBusMessageIter* dbus_msg_iter )
{
    int current_type;
    int arg_count {0};
    while ((current_type = dbus_message_iter_get_arg_type (dbus_msg_iter)) != DBUS_TYPE_INVALID) {
        // std::cout << "  arg[" << arg_count << "] is " << current_type << std::endl;

        if (current_type == DBUS_TYPE_ARRAY) {
            // std::cout << "  DBUS_TYPE_ARRAY : recursing into it" << std::endl;
            // as the array is not of fixed length, must iter recurisively into it
            DBusMessageIter dbus_msg_subiter;
            ::dbus_message_iter_recurse(dbus_msg_iter, &dbus_msg_subiter);

            int subcurrent_type;
            int arg_subcount {0};
            while ((subcurrent_type = dbus_message_iter_get_arg_type (&dbus_msg_subiter)) != DBUS_TYPE_INVALID) {
                // std::cout << "    subarg[" << arg_subcount << "] is " << subcurrent_type << std::endl;

                if (subcurrent_type == DBUS_TYPE_STRUCT ) {
                    // std::cout << "    >> " << "DBUS_TYPE_STRUCT" << std::endl;
                    parse_dbus_inhibitstruct( &dbus_msg_subiter, "      >>" );
                }
                else {
                    std::cout << "    >> not dealt with" << std::endl;
                }

                arg_subcount ++;
                dbus_message_iter_next (&dbus_msg_subiter);
            }
        }
        else {
            std::cerr << "  => arg type not dealt with" << std::endl;
        }
        arg_count ++;
        dbus_message_iter_next (dbus_msg_iter);
    }
}

bool dbus_call_listinhibit()
{
    std::cout << "__Calling ListInhibit" << std::endl;

    // Prepare message
    if ( nullptr == (dbus_msg = ::dbus_message_new_method_call("org.freedesktop.login1", "/org/freedesktop/login1", "org.freedesktop.login1.Manager", "ListInhibitors")) ) {

        std::cerr << "__Error on create message" << std::endl;
        ::dbus_connection_unref(dbus_conn);
        ::perror("ERROR: ::dbus_message_new_method_call - Unable to allocate memory for the message!");
        return false;

        // Invoke remote procedure call, block for response
    } else if ( nullptr == (dbus_reply = ::dbus_connection_send_with_reply_and_block(dbus_conn, dbus_msg, DBUS_TIMEOUT_USE_DEFAULT, &dbus_error)) ) {
        std::cerr << "__Error on call" << std::endl;
        ::dbus_message_unref(dbus_msg);
        ::dbus_connection_unref(dbus_conn);
        ::perror(dbus_error.name);
        ::perror(dbus_error.message);
        return false;
    }

    std::cout << "Connected to D-Bus as \"" << ::dbus_bus_get_unique_name(dbus_conn) << "\"." << std::endl;
    std::cout << "ListInhibit Result:" << std::endl;

    // Iterating trough args as array of (ssssuu)
    DBusMessageIter dbus_msg_iter;
    if ( !::dbus_message_iter_init(dbus_reply, &dbus_msg_iter) ) {//msg is pointer to dbus message received
        std::cerr << "__Iteration : no args" << std::endl;
        ::dbus_message_unref(dbus_reply);
        ::dbus_message_unref(dbus_msg);
        ::dbus_connection_unref(dbus_conn);
        ::perror(dbus_error.name);
        ::perror(dbus_error.message);
        return false;
    }
    else {
        parse_reply_listinhib(&dbus_msg_iter);
    }
    return true;
}

// ***************************************************************************
// ******************************************************************* Inhibit
// ***************************************************************************
bool dbus_call_inhibit()
{
    int dbus_sleep_fd_lock;
    std::cout << "__Calling Inhibit" << std::endl;

    const char *what_str = "sleep";
    const char *who_str = "DBus_inhibit";
    const char *why_str = "for testing purpose";
    const char *mode_str = "block";

    // Prepare message
    if ( nullptr == (dbus_msg = ::dbus_message_new_method_call("org.freedesktop.login1", "/org/freedesktop/login1", "org.freedesktop.login1.Manager", "Inhibit")) ) {

        std::cerr << "__Error on create message" << std::endl;
        ::dbus_connection_unref(dbus_conn);
        ::perror("ERROR: ::dbus_message_new_method_call - Unable to allocate memory for the message!");
        return false;
    }
    // add arguments to message
    else if ( !dbus_message_append_args( dbus_msg,
                                         DBUS_TYPE_STRING, &what_str,
                                         DBUS_TYPE_STRING, &who_str,
                                         DBUS_TYPE_STRING, &why_str,
                                         DBUS_TYPE_STRING, &mode_str,
                                         DBUS_TYPE_INVALID ) ) {

        std::cerr << "__Error on adding args to message" << std::endl;
        ::dbus_connection_unref(dbus_conn);
        ::perror("ERROR: ::dbus_message_new_method_call - Unable to add args to message!");
        return false;
    }
    // Invoke remote procedure call, block for response
    else if ( nullptr == (dbus_reply = ::dbus_connection_send_with_reply_and_block(dbus_conn, dbus_msg, DBUS_TIMEOUT_USE_DEFAULT, &dbus_error)) ) {
        std::cerr << "__Error on call" << std::endl;
        ::dbus_message_unref(dbus_msg);
        ::dbus_connection_unref(dbus_conn);
        ::perror(dbus_error.name);
        ::perror(dbus_error.message);
        return false;
    }
    // Parse response
    else if ( !::dbus_message_get_args(dbus_reply, &dbus_error,
                                       DBUS_TYPE_UNIX_FD, &dbus_sleep_fd_lock,
                                       DBUS_TYPE_INVALID) ) {
        ::dbus_message_unref(dbus_msg);
        ::dbus_message_unref(dbus_reply);
        ::dbus_connection_unref(dbus_conn);
        ::perror(dbus_error.name);
        ::perror(dbus_error.message);
        return false;
    }

    // sucess : tell, wait, unlock
    else {
        std::cout << "Connected to D-Bus as \"" << ::dbus_bus_get_unique_name(dbus_conn) << "\"." << std::endl;
        std::cout << "SLEEP inhibited using fd=" << dbus_sleep_fd_lock << " as lock" << std::endl;

        // wait
        sleep(60*15);
        // release lock
        close(dbus_sleep_fd_lock);
        std::cout << ">> lock released" << std::endl;
    }
    return true;
}

// ***************************************************************************
// ********************************************************************** Main
// ***************************************************************************
int
main (
  int argc,
  char * argv[]
) {
    // (void)argc;
    // (void)argv;

    bool _arg_block {false};
    // std::cout << "__ " << argc << " arguments" << std::endl;
    // for (int i = 0; i < argc; ++i) {
    //     std::cout << "  [" << i << "] : " << argv[i] << std::endl;
    // }
    // stop if the 1 argument is not "block"
    if (argc > 1) {
        if (std::strcmp("block", argv[1]) != 0) {
            std::cerr << "Error : arg " << argv[1] << " unknown" << std::endl;
            std::cerr << "usage : " << argv[0] << " [block]" << std::endl;
            exit(1);
        }
        _arg_block = true;
    }

    // DBusError dbus_error;
    // DBusConnection * dbus_conn = nullptr;
    // DBusMessage * dbus_msg = nullptr;
    // DBusMessage * dbus_reply = nullptr;
    // const char * dbus_result = nullptr;
    // const char * dbus_result_signature = nullptr;

    // Initialize D-Bus error
    ::dbus_error_init(&dbus_error);

    // Connect to D-Bus
    std::cout << "__Connecting..." << std::endl;
    if ( nullptr == (dbus_conn = ::dbus_bus_get(DBUS_BUS_SYSTEM, &dbus_error)) ) {
        std::cerr << "__Error on Connection" << std::endl;
        ::perror(dbus_error.name);
        ::perror(dbus_error.message);
    }
    else if (_arg_block) {
        dbus_call_inhibit();
    }
    else {
        dbus_call_listinhibit();
    }

    // std::cout << "Introspection Result:" << std::endl;

    // // get type of response message
    // int msg_type = ::dbus_message_get_type(dbus_reply);
    // switch (msg_type) {
    //     case DBUS_MESSAGE_TYPE_METHOD_CALL:
    //         std::cout << "__Reply is a METHOD_CALL" << std::endl;
    //         break;
    //     case DBUS_MESSAGE_TYPE_METHOD_RETURN:
    //         std::cout << "__Reply is a METHOD_RETURN" << std::endl;
    //         break;
    //     case DBUS_MESSAGE_TYPE_ERROR:
    //         std::cout << "__Reply is a ERROR" << std::endl;
    //         break;
    //     case DBUS_MESSAGE_TYPE_SIGNAL:
    //         std::cout << "__Reply is a SIGNAL" << std::endl;
    //         break;
    // }

    // // signature from reply
    // dbus_result_signature = ::dbus_message_get_signature(dbus_reply);
    // std::cout << "__Signature " << dbus_result_signature << std::endl;


    ::dbus_message_unref(dbus_msg);
    ::dbus_message_unref(dbus_reply);
    /*
     * Applications must not close shared connections -
     * see dbus_connection_close() docs. This is a bug in the application.
     */
    //::dbus_connection_close(dbus_conn);

    // When using the System Bus, unreference
    // the connection instead of closing it
    ::dbus_connection_unref(dbus_conn);

    // // Parse response : Array
    // } else if ( !::dbus_message_get_args(dbus_reply, &dbus_error, DBUS_TYPE_ARRAY, &dbus_result, DBUS_TYPE_INVALID) ) {
    //     std::cerr << "__Error on parsing response" << std::endl;
    //     ::dbus_message_unref(dbus_msg);
    //     ::dbus_message_unref(dbus_reply);
    //     ::dbus_connection_unref(dbus_conn);
    //     ::perror(dbus_error.name);
    //     ::perror(dbus_error.message);
    // // Work with the results of the remote procedure call
    // } else {
    //     std::cout << "Connected to D-Bus as \"" << ::dbus_bus_get_unique_name(dbus_conn) << "\"." << std::endl;
    //     std::cout << "Introspection Result:" << std::endl;
    //     std::cout << std::endl << dbus_result << std::endl << std::endl;
    //     ::dbus_message_unref(dbus_msg);
    //     ::dbus_message_unref(dbus_reply);

    //     /*
    //      * Applications must not close shared connections -
    //      * see dbus_connection_close() docs. This is a bug in the application.
    //      */
    //     //::dbus_connection_close(dbus_conn);

    //     // When using the System Bus, unreference
    //     // the connection instead of closing it
    //     ::dbus_connection_unref(dbus_conn);
    // }

    return 0;
}
